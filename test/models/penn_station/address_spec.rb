require 'test_rails_helper'

module PennStation
  describe Address do

    let(:subject) do
      Address.new(
        address1:   "123 Main St",
        address2:   "Suite 100",
        city:       "Baltimore",
        state:      "MD",
        zipcode:    "21229",
        directions: "top of the stairs"
      )
    end

    describe "is valid when" do
      it "has an address1" do
        subject.must_respond_to(:address1)
      end

      it "has an address2" do
        subject.must_respond_to(:address2)
      end

      it "has a city" do
        subject.must_respond_to(:city)
      end

      it "has a state" do
        subject.must_respond_to(:state)
      end

      it "has a zipcode" do
        subject.must_respond_to(:zipcode)
      end

      it "has directions" do
        subject.must_respond_to(:directions)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "is missing address1" do
        subject.address1 = nil
        subject.valid?.wont_equal true
      end

      it "is missing city" do
        subject.city = nil
        subject.valid?.wont_equal true
      end

      it "is missing state" do
        subject.state = nil
        subject.valid?.wont_equal true
      end

      it "is missing zipcode" do
        subject.zipcode = nil
        subject.valid?.wont_equal true
      end
    end

    it "generates an activity message" do
      subject.activity_info.must_equal("address #{subject.address1}")
    end
  end
end
