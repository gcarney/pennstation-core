require 'test_rails_helper'

module PennStation
  describe User do

    let(:subject) do
      User.new(
        email: "joe@test.com",
        name: "Joe Test",
        password: "qwerty123",
        password_confirmation: "qwerty123"
      )
    end

    describe "is valid when" do
      it "has associated roles" do
        subject.must_respond_to(:roles)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    it "can tell if a user has a role" do
      subject.roles << Role.new(name: "editor", user: nil)
      subject.has_role?("admin").must_equal false
    end

    it "has an STI type" do
      subject.save!
      subject.reload
      subject.type.must_equal "PennStation::User"
    end

    it "generates an activity message" do
      subject.activity_info.must_equal "user #{subject.name}"
    end
  end
end
