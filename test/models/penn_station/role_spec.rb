require 'test_rails_helper'

module PennStation
  describe Role do

    let(:subject) {
      Role.new(
        name: 'admin'
      )
    }

    describe "when it is valid" do
      it "should have a name" do
        subject.must_respond_to(:name)
      end

      it "has an user" do
        subject.must_respond_to(:user)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe ".available" do
      it "returns an array of all available roles" do
        Role.available.must_equal Role::LIST
      end
    end
  end
end
