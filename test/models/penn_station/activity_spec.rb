require 'test_rails_helper'

module PennStation
  describe Activity do

    let(:subject) do
      Activity.new(
        user: "user name",
        info: "page 'foo'",
        operation: "created"
      )
    end

    describe "is valid when" do
      it "has an user" do
        subject.must_respond_to(:user)
      end

      it "has an operation type" do
        subject.must_respond_to(:operation)
      end

      it "has info" do
        subject.must_respond_to(:info)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "is missing user" do
        subject.user = nil
        subject.valid?.wont_equal true
      end

      it "is missing operation" do
        subject.operation = nil
        subject.valid?.wont_equal true
      end

      it "is missing info" do
        subject.info = nil
        subject.valid?.wont_equal true
      end
    end

    it "returns a message containing all info" do
      subject = Activity.new(user: "user name", operation: "created", info: "page 'foo'")
      subject.message.must_equal("#{subject.user} #{subject.operation} #{subject.info}")
    end
  end
end
