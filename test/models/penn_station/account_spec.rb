require 'test_rails_helper'

module PennStation
  describe Account do

    let(:subject) do
      Account.new(
        email: "joe@test.com",
        name: "Joe Test",
        password: "qwerty123",
        password_confirmation: "qwerty123"
      )
    end

    describe "is valid when" do
      it "has a name" do
        subject.must_respond_to(:name)
      end

      it "has a email" do
        subject.must_respond_to(:email)
      end

      it "has a password" do
        subject.must_respond_to(:password)
      end

      it "has a password_confirmation" do
        subject.must_respond_to(:password_confirmation)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "has a short password after being created" do
        subject.save
        subject.reload

        subject.password = "1"
        subject.password_confirmation = "1"

        subject.valid?.wont_equal true
      end

      it "is missing email" do
        subject.email = nil
        subject.valid?.wont_equal true
      end

      it "is missing name" do
        subject.name = nil
        subject.valid?.wont_equal true
      end

      it "has a duplicate email" do
        subject.save
        subject_dup = Account.new(
          email: "joe@test.com",
          name: "Joe Test",
          password: "qwerty123",
          password_confirmation: "qwerty123"
        )

        subject_dup.valid?.wont_equal true
      end
    end

    it "generates an activity message" do
      subject.activity_info.must_equal "account #{subject.name}"
    end
  end
end
