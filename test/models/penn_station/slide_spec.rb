require 'test_rails_helper'

module PennStation
  describe Slide do

    let(:an_image) { File.open(File.join(Rails.root, '/fixtures/files/check.png')) }

    let(:subject) do
      Slide.new(
        title: "MyString",
        caption: "MyString",
        description: "MyString",
        url: "MyString",
        publish: false,
        position: 1,

        image: an_image,
        size: 1,
        content_type: "MyString",

        slideshow: nil
      )
    end

    describe "is valid when" do
      it "has a caption" do
        subject.must_respond_to(:caption)
      end

      it "has a content type" do
        subject.must_respond_to(:content_type)
      end

      it "has a description" do
        subject.must_respond_to(:description)
      end

      it "has an image" do
        subject.must_respond_to(:image)
      end

      it "has a position" do
        subject.must_respond_to(:position)
      end

      it "has a publish flag" do
        subject.must_respond_to(:publish)
      end

      it "has a size" do
        subject.must_respond_to(:size)
      end

      it "has a slideshow" do
        subject.must_respond_to(:slideshow)
      end

      it "has a title" do
        subject.must_respond_to(:title)
      end

      it "has a url" do
        subject.must_respond_to(:url)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "is missing a title" do
        subject.title = nil
        subject.valid?.wont_equal true
      end
    end
  end
end
