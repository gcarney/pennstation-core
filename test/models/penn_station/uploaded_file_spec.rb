require 'test_rails_helper'

module PennStation
  describe UploadedFile do

    let(:folder) do
      Folder.new(
        name: "MyString"
      )
    end

    let(:subject) do
      UploadedFile.new(
        label:       "MyString",
        caption:     "MyText",
        description: "MyText",
        folder: folder,
        file: true
      )
    end

    describe "is valid when" do
      it "has a folder" do
        subject.must_respond_to(:folder)
      end
    end

    it "returns Activity record info" do
      subject.activity_info.must_equal "uploaded file to folder #{folder.name}"
    end
  end
end
