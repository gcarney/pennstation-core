require 'test_rails_helper'

module PennStation
  describe Section do

    let(:subject) { Section.new }

    let(:page) do
      Page.new(
        title: "MyString",
        url: nil,
        ptype: "page",
        position: 1,
        published: false,
        show_in_nav: false
      )
    end

    describe "is valid when" do
      it "has a name" do
        subject.must_respond_to(:name)
      end

      it "has content" do
        subject.must_respond_to(:content)
      end

      it "has an stype" do
        subject.must_respond_to(:stype)
      end

      it "has a position" do
        subject.must_respond_to(:position)
      end

      it "has a published flag" do
        subject.must_respond_to(:published)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid" do
      it "has a position less than 0" do
        subject.position = -1
        subject.valid?.wont_equal true
      end
    end

    describe "is not valid on existing records when" do
      it "is missing content" do
        subject.save!
        subject.reload
        subject.content = nil

        subject.valid?.wont_equal true
      end

      it "is missing a name" do
        subject.save!
        subject.reload
        subject.name = nil
        subject.valid?.wont_equal true
      end

      it "is missing a type" do
        subject.save!
        subject.reload
        subject.stype = nil

        subject.valid?.wont_equal true
      end
    end

    it "sets a default stype on a new record" do
      subject.valid?
      subject.stype.must_equal "Section"
    end

    it "sets a default position" do
      subject.valid?
      subject.position.must_equal 1
    end

    it "sets a default for published on a new record" do
      subject.valid?
      subject.published.must_equal true
    end

    describe "default content" do
      it "sets default content on a non-form section" do
        subject.valid?
        subject.content.must_equal "<h1>#{subject.name.humanize} Section Content</h1>"
      end

      it "sets default content on a form section" do
        subject.stype='Form'
        subject.valid?
        subject.content.must_equal subject.form_defaults.to_json
      end

      it "sets default content on a contact form section" do
        subject.stype='ContactForm'
        subject.valid?
        subject.content.must_equal subject.contact_form_defaults.to_json
      end
    end

    describe 'form sections' do
      describe "#is_related_to_form?" do
        it "is true when the section's stype ends in 'FormError'" do
          subject.stype = 'FooFormError'
          subject.is_related_to_form?.must_equal true
        end

        it "is true when the section's stype ends in 'FormSuccess'" do
          subject.stype = 'FooFormSuccess'
          subject.is_related_to_form?.must_equal true
        end

        it "is false when the section's stype equals 'Form'" do
          subject.stype = 'Form'
          subject.is_related_to_form?.must_equal false
        end
      end

      describe "#is_form?" do
        it "is true when the section's stype equals 'Form'" do
          subject.stype = 'Form'
          subject.is_form?.must_equal true
        end

        it "is true when the section's stype ends in 'Form'" do
          subject.stype = 'ContactForm'
          subject.is_form?.must_equal true
        end

        it "is true when a prefix is passed" do
          subject.stype = 'ContactForm'
          subject.is_form?('Contact').must_equal true
        end

        it "is not true when the section's stype does not end in 'Form'" do
          subject.stype = 'Section'
          subject.is_form?.must_equal false
        end
      end
    end

    describe "after_create" do
      it "adds success and fail sections for a form section" do
        subject.name = 'Get Down'
        subject.stype = 'Form'
        subject.save!

        s = PennStation::Section.find_by_stype('FormSuccess')
        s.wont_equal nil
        s.position.must_equal 0

        s = PennStation::Section.find_by_stype('FormError')
        s.wont_equal nil
        s.position.must_equal 0
      end
    end

    it "returns Activity record info" do
      subject.page = page
      subject.activity_info.must_equal "page #{page.title}, section #{subject.name}"
    end
  end
end
