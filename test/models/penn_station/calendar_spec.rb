require 'test_rails_helper'

module PennStation
  describe Calendar do
    let(:subject) { Calendar.new( name: "MyString") }

    let(:scheduled_event) do
      ScheduledEvent.new(
        starts_at: Date.yesterday,
        ends_at:   Date.today,
        times:     "1:00pm - 2:00pm"
      )
    end

    describe "is valid when" do
      it "has a name" do
        subject.must_respond_to(:name)
      end

      it "has associated scheduled events" do
        subject.must_respond_to(:scheduled_events)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "is missing name" do
        subject.name = nil
        subject.valid?.wont_equal true
      end
    end

    it "allows a scheduled event to be associated" do
      subject.scheduled_events = [ scheduled_event ]
      subject.save
      subject.reload

      subject.scheduled_events.must_equal [ scheduled_event ]
    end

    it "should return Activity record info" do
      subject.activity_info.must_equal "calendar #{subject.name}"
    end
  end
end
