require 'test_rails_helper'

module PennStation
  describe Slideshow do

    let(:subject) do
      Slideshow.new(
        name: "MyString",
        image_size_note: "size note"
      )
    end

    describe "is valid when" do
      it "has a name" do
        subject.must_respond_to(:name)
      end

      it "has an image_size_note" do
        subject.must_respond_to(:image_size_note)
      end

      it "has slides" do
         subject.must_respond_to(:slides)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "is missing a name" do
        subject.name = nil
        subject.valid?.wont_equal true
      end
    end

    it "returns Activity record info" do
      subject.activity_info.must_equal "slideshow #{subject.name}"
    end
  end
end
