require 'test_rails_helper'

module PennStation
  describe Folder do

    let(:subject) do
      Folder.new(
        name: "MyString"
      )
    end

    describe "is valid when" do
      it "has a name" do
        subject.must_respond_to(:name)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "is missing a name" do
        subject.name = nil
        subject.valid?.wont_equal true
      end
    end

    it "generates an activity message" do
      subject.activity_info.must_equal "folder #{subject.name}"
    end
  end
end
