require 'test_rails_helper'

module PennStation
  describe Event do

    let(:subject) do
      Event.new(
        name:        "MyString",
        summary:     "MyString",
        description: "MyString"
      )
    end

    describe "is valid when" do
      it "has a name" do
        subject.must_respond_to(:name)
      end

      it "has a summary" do
        subject.must_respond_to(:summary)
      end

      it "has a description" do
        subject.must_respond_to(:description)
      end

      it "has locations" do
        subject.must_respond_to(:locations)
      end

      it "has scheduled events" do
        subject.must_respond_to(:scheduled_events)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end

    describe "is not valid when" do
      it "is missing name" do
        subject.name = nil
        subject.valid?.wont_equal true
      end

      it "is missing summary" do
        subject.summary = nil
        subject.valid?.wont_equal true
      end

      it "is missing a description" do
        subject.description = nil
        subject.valid?.wont_equal true
      end
    end

    it "returns Activity record info" do
      subject.activity_info.must_equal "event #{subject.name}"
    end
  end
end
