require 'test_rails_helper'

module PennStation
  describe FormSubmission do

    let(:subject) do
      FormSubmission.new(
        submitted_values: "created"
      )
    end

    describe "is valid when" do
      it "has submitted_values" do
        subject.must_respond_to(:submitted_values)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end
    end
  end
end
