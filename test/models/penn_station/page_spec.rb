require 'test_rails_helper'

module PennStation
  describe Page do

    let(:subject) do
      Page.new(
        title: "MyString",
        url: nil,
      )
    end

    let(:subject_published) do
      Page.new(
        title: "MyString",
        url: nil,
        published: true
      )
    end

    let(:subject_home_page) do
      Page.new(
        title: "Home",
        ptype: "HomePage",
        url: nil,
        published: false
      )
    end

    describe "when a page is valid" do
      it "has a title" do
        subject.must_respond_to(:title)
      end

      it "has a url" do
        subject.must_respond_to(:url)
      end

      it "has a parent_url" do
        subject.must_respond_to(:parent_url)
      end

      it "has a position" do
        subject.must_respond_to(:position)
      end

      it "has a published flag" do
        subject.must_respond_to(:published)
      end

      it "has sections" do
        subject.must_respond_to(:sections)
      end

      it "has a show_in_nav flag" do
        subject.must_respond_to(:show_in_nav)
      end

      it "has a show_share_widget flag" do
        subject.must_respond_to(:show_share_widget)
      end

      it "has a page type" do
        subject.must_respond_to(:ptype)
      end

      it "has a password_protected flag" do
        subject.must_respond_to(:password_protected)
      end

      it "is valid" do
        subject.valid?.must_equal true
      end

      it "should return activity info" do
        subject.must_respond_to(:activity_info)
      end

      it "should have at least one section" do
        subject.save
        subject.sections.count.must_equal 1
      end
    end

    describe "is not valid when" do
      it "is missing a title" do
        subject.title = nil
        subject.valid?.wont_equal true
      end

      it "has a position less than 0" do
        subject.position = -1
        subject.valid?.wont_equal true
      end

      it "s has non-alphnumeric characters in the page type" do
        subject.ptype = 'A B'
        subject.valid?.wont_equal true
      end

      it "has a duplicate title" do
        subject2 = subject.dup
        subject.save!

        subject2.save.wont_equal true
      end
    end

    it "sets a default for position on a new record" do
      subject.valid?
      subject.position.must_equal 0
    end

    it "sets a default for password_protected on a new record" do
      subject.valid?
      subject.password_protected.must_equal false
    end

    it "sets a default for published on a new record" do
      subject.valid?
      subject.published.must_equal false
    end

    it "sets a default for show_share_widget on a new record" do
      subject.valid?
      subject.show_share_widget.must_equal false
    end

    it "uses assigned published on a new record" do
      subject_published.valid?
      subject_published.published.must_equal true
    end

    it "always sets home page to be published on a new record" do
      subject_home_page.valid?
      subject_home_page.published.must_equal true
    end

#    describe "#seo_description length" do
#      it "is less than 155 charavters" do
#        subject.seo_description = 'x' * 156
#        subject.valid?.wont_equal true
#      end
#    end

#    context "a page" do
#      let(:page_title) { 'the title' }
#
#      context "when it is creates" do
#        let(:p_url) { '/a' }
#        let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: p_url) }
#
#        it "should be locatable by url" do
#          page2 = PennStation::Page.lookup page.url[1..-1]
#          expect(page2.url).to eq(page.url)
#        end
#
#        it "should return Activity record info" do
#          expect(page.activity_info).to eq("page #{page.title}")
#        end
#      end
#    end
#
#    context "a page's url" do
#      let(:page_title) { 'the title' }
#
#      context "when it is under the home page" do
#        let(:p_url) { '/a' }
#        let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: p_url) }
#
#        it "should be looked up by url" do
#          page2 = PennStation::Page.lookup page.url[1..-1]
#          expect(page2.url).to eq(page.url)
#        end
#
#        it "should include the parent_url after saving" do
#          page2 = PennStation::Page.find page.id
#          expect(page2.url).to match(/^#{page2.parent_url}\/.+/)
#        end
#
#        it "should include the parent_url after loading" do
#          page2 = PennStation::Page.find page.id
#          expect(page2.parent_url).to eq(p_url)
#        end
#      end
#    end
#
#    context "when it is the HomePage" do
#      let(:page_title) { 'HomePage' }
#      let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: '') }
#
#      it "should be published" do
#        page2 = PennStation::Page.find page.id
#        expect(page2.published).to be_true
#      end
#
#      it "should be of type 'HomePage'" do
#        page2 = PennStation::Page.find page.id
#        expect(page2.ptype).to eq("HomePage")
#      end
#
#      it "should be recognized as the home page" do
#        page2 = PennStation::Page.find page.id
#        expect(page2.home_page?).to be_true
#      end
#
#      context "url" do
#        it "should be the root" do
#          page2 = PennStation::Page.find page.id
#          expect(page2.url).to eq("/")
#        end
#      end
#
#      context "parent_url" do
#        it "should be empty" do
#          page2 = PennStation::Page.find page.id
#          expect(page2.parent_url).to be_empty
#        end
#      end
#
#      context "#parent" do
#        let(:home_page) { FactoryGirl.create(:penn_station_page, title: 'Home Page', ptype: 'HomePage', parent_url: '') }
#        let(:first_level_page) { FactoryGirl.create(:penn_station_page, title: 'About', parent_url: home_page.url) }
#
#        it "first level pages should return the home page" do
#          expect(first_level_page.parent).to eq(home_page)
#        end
#      end
#
#      context ".top_level" do
#        let(:home_page) { FactoryGirl.create(:penn_station_page, title: 'Home Page', ptype: 'HomePage', parent_url: '') }
#        let(:first_level_page) { FactoryGirl.create(:penn_station_page, title: 'About', parent_url: home_page.url) }
#        let(:second_level_page) { FactoryGirl.create(:penn_station_page, title: 'About Us', parent_url: first_level_page.url) }
#
#        it "first level pages should return themselves" do
#          expect(PennStation::Page.top_level(first_level_page)).to eq(first_level_page)
#        end
#
#        it "first level pages should return themsels" do
#          expect(PennStation::Page.top_level(second_level_page)).to eq(first_level_page)
#        end
#      end
#    end
#
#    context "a first-level sub-page" do
#      let(:page_title) { 'sub1' }
#      let(:p_url) { '/' }
#      let(:page) { FactoryGirl.create(:penn_station_page, title: page_title, parent_url: p_url) }
#
#      it "should not be published" do
#        page2 = PennStation::Page.find page.id
#        expect(page2.published).to be_false
#      end
#
#      context "url" do
#        it "should be under the root" do
#          page2 = PennStation::Page.find page.id
#          expect(page2.url).to eq("/sub1")
#        end
#      end
#
#      context "parent_url" do
#        it "should not be empty" do
#          page2 = PennStation::Page.find page.id
#          expect(page2.parent_url).to eq("/")
#        end
#      end
#    end
  end
end
