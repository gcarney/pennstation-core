require 'test_rails_helper'

module PennStation
  describe FormSubmissionMailer do
    let(:recipients) { %w( user1@test.com user2@test.com ) }
    let(:visitor) { 'websiteuser@test.com' }
    let(:form_info) {
      OpenStruct.new(
        get_email: visitor,
        recipients: recipients,
        submitted_values: { name: "_NAME_", email: "foo@test.com" }.to_json
      )
    }

    before do
      ActionMailer::Base.deliveries.clear
    end

    it "emails the administrators" do
      page_title = "Contact"

      # Send the email, then test that it got queued
      email = FormSubmissionMailer.notify_administrators(page_title, form_info).deliver_now
      ActionMailer::Base.deliveries.wont_be_empty

      # Test the body of the sent email contains what we expect it to
      email.from.must_equal Array(visitor)
      email.to.must_equal Array(recipients)
      email.subject.must_equal "Website form received: #{page_title}"
#      assert_equal read_fixture('invite').join, email.body.to_s
    end

    it "emails the visitor" do
      # Send the email, then test that it got queued
      email = FormSubmissionMailer.notify_visitor(form_info).deliver_now
      ActionMailer::Base.deliveries.wont_be_empty

      # Test the body of the sent email contains what we expect it to
      email.from.must_equal Array(recipients)
      email.to.must_equal Array(visitor)
      email.subject.must_equal "Your message was received"
      email.body.to_s.must_equal IO.readlines(File.join(Rails.root, 'fixtures', self.class.name.underscore, 'notify_visitor')).join
    end
  end
end
