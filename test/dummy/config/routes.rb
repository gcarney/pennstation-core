Rails.application.routes.draw do

  mount PennStation::Engine => "/admin"

  root :to => 'pages#show', :url => '/'

#  get "logout" => "admin/sessions#destroy", :as => "logout"
#  get "login"  => "admin/sessions#new",     :as => "login"

  match ':url' => 'pages#show', :via => :get
end
