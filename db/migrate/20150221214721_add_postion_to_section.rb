class AddPostionToSection < ActiveRecord::Migration
  def change
    add_column :penn_station_sections, :position, :integer
  end
end
