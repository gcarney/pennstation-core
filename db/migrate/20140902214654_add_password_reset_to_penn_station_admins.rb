class AddPasswordResetToPennStationAdmins < ActiveRecord::Migration
  def change
    add_column :penn_station_admins, :password_reset_token, :string
    add_column :penn_station_admins, :password_reset_sent_at, :datetime
  end
end
