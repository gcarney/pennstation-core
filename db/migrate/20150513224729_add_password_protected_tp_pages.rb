class AddPasswordProtectedTpPages < ActiveRecord::Migration
  def change
    add_column :penn_station_pages, :password_protected, :boolean, default: false
  end
end
