class RenamePennStationRolesAdminId < ActiveRecord::Migration
  def change
    rename_column :penn_station_roles, :admin_id, :user_id
  end
end
