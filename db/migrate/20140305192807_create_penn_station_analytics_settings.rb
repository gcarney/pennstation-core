class CreatePennStationAnalyticsSettings < ActiveRecord::Migration
  def change
    create_table :penn_station_analytics_settings do |t|
      t.string :tracking_id
      t.string :view_id
      t.text   :tracking_code
      t.string :access_token
      t.string :refresh_token
      t.string :expires_at

      t.timestamps
    end
  end
end
