class CreatePennStationCalendars < ActiveRecord::Migration
  def change
    create_table :penn_station_calendars do |t|
      t.string :name

      t.timestamps
    end
  end
end
