class AddMoreinfoUrlFieldToSections < ActiveRecord::Migration
  def change
    add_column :penn_station_sections, :moreinfo_url, :string
  end
end

