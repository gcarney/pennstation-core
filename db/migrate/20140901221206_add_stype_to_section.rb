class AddStypeToSection < ActiveRecord::Migration
  def change
    add_column :penn_station_sections, :stype, :string
  end
end
