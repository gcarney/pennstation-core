class CreatePennStationFormSubmissions < ActiveRecord::Migration
  def change
    create_table :penn_station_form_submissions do |t|
      t.text :submitted_values

      t.timestamps
    end
  end
end
