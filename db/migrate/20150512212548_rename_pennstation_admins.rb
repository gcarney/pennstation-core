class RenamePennstationAdmins < ActiveRecord::Migration
  def change
    rename_table :penn_station_admins, :penn_station_accounts
  end
end
