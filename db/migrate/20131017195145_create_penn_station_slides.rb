class CreatePennStationSlides < ActiveRecord::Migration
  def change
    create_table :penn_station_slides do |t|
      t.string     :title
      t.string     :caption
      t.text       :description
      t.string     :url
      t.boolean    :publish
      t.integer    :position

      t.references :slideshow

      t.string     :image
      t.integer    :size
      t.string     :content_type

      t.timestamps
    end
  end
end
