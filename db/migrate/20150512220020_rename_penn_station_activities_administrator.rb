class RenamePennStationActivitiesAdministrator < ActiveRecord::Migration
  def change
    rename_column :penn_station_activities, :administrator, :user
  end
end
