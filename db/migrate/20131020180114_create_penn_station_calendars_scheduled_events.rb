class CreatePennStationCalendarsScheduledEvents < ActiveRecord::Migration
  def change
    create_table :penn_station_calendars_scheduled_events do |t|
      t.references :calendar
      t.references :scheduled_event

      t.timestamps
    end
  end
end
