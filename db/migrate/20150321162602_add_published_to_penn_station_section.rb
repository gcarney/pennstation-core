class AddPublishedToPennStationSection < ActiveRecord::Migration
  def change
    add_column :penn_station_sections, :published, :boolean
  end
end
