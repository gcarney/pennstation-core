class AddAuthTokenToPennStationAdmins < ActiveRecord::Migration
  def change
    add_column :penn_station_admins, :auth_token, :string
  end
end
