
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "penn_station/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "penn_station"
  s.version     = PennStation::VERSION
  s.authors     = ["warren vosper"]
  s.email       = ["straydogsw@gmail.com"]
  s.homepage    = "http:://cctsbaltimore.org"
  s.summary     = "PennStation Content Management System (CMS)."
  s.description = "PennStation implemented via a Rails engine."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.1"
  s.add_dependency "jquery-rails"
  s.add_dependency "bcrypt", '~> 3.1.9'
  s.add_dependency "dynamic_form"
  s.add_dependency "rubytree"
  #s.add_dependency "tinymce-rails", '=4.1.6'
  s.add_dependency "tinymce-rails", '=3.5.8.2'
  s.add_dependency "sass-rails",   '~> 5.0.0'
  s.add_dependency "dropzonejs-rails"
  s.add_dependency "rmagick"
  s.add_dependency "carrierwave", '=0.10.0'
  s.add_dependency "legato", '=0.4.0'
  s.add_dependency "oauth2", '=1.0.0'
  s.add_dependency "autoprefixer-rails"
end
