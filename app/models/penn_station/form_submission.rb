module PennStation
  class FormSubmission < ActiveRecord::Base
    attr_accessor :field_validations, :recipients

    def fields_valid?(security_question_correct)
      unless security_question_correct
        errors.add :security_question, "is incorrect."
      end

      field_validations.keys.each do |f|
        next unless %w( presence ).include?(field_validations[f])

        unless self.send("get_#{f}").present?
          errors.add f.to_sym, "cannot be empty."
        end
      end

      errors.empty?
    end

    def activity_info
      "form submission #{id}"
    end

    def method_missing(meth, *args, &block)
      if meth.to_s == 'recipients'
        JSON.parse(submitted_values).fetch('recipients', 'NO.RECIPIENTS@test.com')
      else
        (meth.to_s =~ /^get_(.+)$/) ? JSON.parse(submitted_values).fetch($1, '???') : super
      end
    end
  end
end
