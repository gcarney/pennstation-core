module PennStation
  class Page < ActiveRecord::Base
    attr_accessor :parent_url

    validates :position, presence: true,
                         numericality: { :greater_than_or_equal_to => 0 }

    validates :ptype, format: { :with => /\A[a-zA-Z\d]+\z/ }

    validates :published, inclusion: { :in => [true, false] }

    validates :seo_description, :length => { :maximum => 155 }

    validates :show_in_nav, inclusion: { :in => [true, false] }

    validates :title, presence: true,
                      uniqueness: true

    validates :url, presence: true,
                    uniqueness: true

    before_validation :urlize

    before_validation :set_defaults, :on => :create

    after_find do |page|
      if ptype == 'HomePage'
        self.parent_url = ''
      else
        parent = url.split('/')[0..-2].join('/')
        parent = '/' if parent.empty?
        self.parent_url = parent
      end
    end

    has_many :sections, :dependent => :destroy

    scope :with_password_protection, -> { where(password_protected: true) }

    def self.lookup(url)
      self.find_by_url((url == '/') ? '/' : "/#{url}")
    end

    def self.top_level(page)
      return page if page.parent_url == '/' or page.url == '/'
      self.where(url: '/' + page.url.split('/')[1]).first
    end

    def parent
      Page.find_by_url(parent_url)
    end

    def home_page?
      url == '/'
    end

    def has_parent?
      not parent_url.empty?
    end

    def activity_info
      "page #{title}"
    end

    def portal(page=self)
      return nil unless page.present?
      return page if page.password_protected?
      portal(page.parent)
    end

    private

    def urlize
      if (ptype == 'HomePage')
        self.url = '/'
      else
        self.url = (parent_url || '/')
        self.url += '/' unless url[-1] == '/'
        self.url += (title || '').gsub(/ /, "_").gsub(/[^\d\w\-_]/, "").downcase
      end
    end

    def set_defaults
      self.ptype = ptype || (home_page?  ? 'HomePage' : 'Page')
      self.published = published || false
      self.published = true if home_page?
      self.show_in_nav = show_in_nav || true
      self.show_share_widget = show_share_widget || false
      self.sections << Section.new
      self.position = position || 0
      self.password_protected = password_protected || false

      true
    end
  end
end
