
module PennStation
  class Media
    def self.all
      Folder.all.order(:name).includes(:uploaded_files) +
      Slideshow.all.order(:name).includes(:slides)
    end
  end
end
