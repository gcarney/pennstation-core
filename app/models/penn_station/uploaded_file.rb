require 'file_size_validator'

module PennStation
  class UploadedFile < ActiveRecord::Base
    mount_uploader :file, MediaUploader

    validates  :file, :presence => true,
                      :file_size => { :maximum => UPLOADED_FILE_MAX_SIZE }

    belongs_to :folder

    before_save :update_file_attributes

    def activity_info
      "uploaded file to folder #{folder.name}"
    end

    def filename
      File.basename(file.path.gsub(/\W+/,'_'))
    end

    private

    def update_file_attributes
      if file.present? && file_changed?
        self.content_type = file.file.content_type
        self.size = file.file.size
      end
    end
  end
end
