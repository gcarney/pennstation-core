module PennStation
  class ScheduledEvent < ActiveRecord::Base
    has_and_belongs_to_many :calendars, class_name: "::PennStation::Calendar"
    belongs_to              :event,     class_name: "::PennStation::Event"

    accepts_nested_attributes_for :calendars

    scope :next, -> (num) { where('ends_at > ?', Date.yesterday).order(:starts_at).limit(num) }

    def status
      return 'Upcoming' if starts_at > Date.today
      return 'Closed' if ends_at < Date.today
      'Current'
    end
  end
end
