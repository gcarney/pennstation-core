module PennStation
  class Folder < ActiveRecord::Base
    validates :name, presence: true,
                     uniqueness: true

    has_many  :uploaded_files

    around_update :rename_media_folder_if_name_change

    def activity_info
      "folder #{name}"
    end

    private

    def rename_media_folder_if_name_change
      yield

      if name_changed?
        folders_dir = 'public/penn_station/folders'

        if File.exist? "#{folders_dir}/#{name_change[0]}"
          File.rename "#{folders_dir}/#{name_change[0]}", "#{folders_dir}/#{name_change[1]}"
        end
      end
    end
  end
end
