
module PennStation
  class Calendar < ActiveRecord::Base
    validates :name, presence: true, uniqueness: true

    has_and_belongs_to_many :scheduled_events,
                            class_name: "::PennStation::ScheduledEvent"

    def activity_info
      "calendar #{name}"
    end
  end
end
