module PennStation
  class Role < ActiveRecord::Base

    LIST = [ "editor", "admin", "pennstation_admin" ]

    validates_associated :user

    validates :name, presence: true,
                     inclusion: { :in => LIST }

    belongs_to :user

    def self.available
      LIST
    end

    def assignable_roles(admin)

        # order matters here - most powerful to least powerful

      return LIST if admin.roles.map(&:name).include?("pennstation_admin")
      return [ "editor", "admin" ] if admin.roles.map(&:name).include?("admin")
      []
    end
  end
end
