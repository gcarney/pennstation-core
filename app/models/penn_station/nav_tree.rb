require 'rubytree'

module PennStation
  class NavTree < PageTree
    private

    def create_nodes
      Page.where("url != '/'").where(published: true, show_in_nav: true).order(:url).each do |page|
        add_page(page)
      end
    end
  end
end
