require 'file_size_validator'

module PennStation
  class Slide < ActiveRecord::Base
    mount_uploader :image, SlideUploader

    validates :title, presence: true
    validates :image, presence: true,
                      file_size: { :maximum => SLIDE_MAX_SIZE }

    belongs_to  :slideshow

    before_save :update_file_attributes

    def activity_info
      "uploaded image to slideshow #{slideshow.name}"
    end

    private

    def update_file_attributes
      if image.present? && image_changed?
        self.content_type = image.file.content_type
        self.size = image.file.size
      end
    end
  end
end
