
module PennStation
  class Event < ActiveRecord::Base
    validates :description, presence: true
    validates :name, presence: true
    validates :summary, presence: true

    has_many :scheduled_events, class_name: "::PennStation::ScheduledEvent"
    has_many :locations

    accepts_nested_attributes_for :locations
    accepts_nested_attributes_for :scheduled_events

    def has_calendar?(calendar)
      scheduled_events.map{|m| m.calendars.map{|m2| m2.name}}.flatten.include?(calendar.name)
    end

    def activity_info
      "event #{name}"
    end
  end
end
