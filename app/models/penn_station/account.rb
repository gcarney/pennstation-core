
# http://asciicasts.com/episodes/270-authentication-in-rails-3-1

module PennStation
  class Account < ActiveRecord::Base
    has_secure_password

    validates :email, presence: true,
                      uniqueness: true

    validates :name, :presence => true,
                     :uniqueness => true

    validates :password, :length => { :minimum => 8, :if => :validate_password? },
                         :confirmation => { :if => :validate_password? }

    validates :password_confirmation, :length => { :minimum => 8, :if => :validate_password? }

    before_create { generate_token(:auth_token) }
    before_validation :generate_random_password, :on => :create

    def activity_info
      "account #{name}"
    end

    def send_password_reset(mailer=UserMailer)
      generate_token(:password_reset_token)
      self.password_reset_sent_at = Time.zone.now
      save!
      mailer.password_reset(self).deliver#_later
    end

    def generate_token(column)
      begin
        self[column] = SecureRandom.urlsafe_base64
      end while Account.exists?(column => self[column])
    end

    private

    def generate_random_password
      self.password = self.password_confirmation = (0...8).map { (65 + rand(26)).chr }.join
    end

    def validate_password?
      !password.nil? || !password_confirmation.nil?
    end
  end
end
