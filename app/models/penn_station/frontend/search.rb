class Search < ActiveRecord::Base
  def self.results(search_term, search_protected_pages = false)
    @results = {}

    pages = PennStation::Page.arel_table
    sections = PennStation::Section.arel_table

    @results[:pages] = PennStation::Page.joins(:sections)
      .where(pages[:published].eq(true))
      .where(pages[:url].does_not_match(PennStation::PROTECTED_PAGES))
      .where(sections[:content].matches("%#{search_term}%"))
      .uniq.to_a

    if defined?(PennStationBlog)
      posts = PennStationBlog::Post.arel_table

      @results[:blog_posts] = PennStationBlog::Post
        .where(posts[:publish].eq(true))
        .where(["title LIKE ? OR summary LIKE ? OR body LIKE ?",
               "%#{search_term}%", "%#{search_term}%", "%#{search_term}%"])
        .to_a
    end

    if search_protected_pages && PennStation::PROTECTED_PAGES.present?
      @results[:pages] += PennStation::Page.joins(:sections)
        .where(pages[:published].eq(true))
        .where(sections[:content].matches("%#{search_term}%"))
        .where(pages[:url].matches(PennStation::PROTECTED_PAGES))
        .uniq.to_a
    end

    @results
  end
end
