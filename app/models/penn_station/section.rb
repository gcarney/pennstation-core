module PennStation
  class Section < ActiveRecord::Base
    validates :content,  presence: true
    validates :name,     presence: true
    validates :position, presence: true,
                         numericality: { :greater_than_or_equal_to => 0 }

    validates :stype,    presence: true

    before_validation :set_defaults, :on => :create
    after_create      :add_related_form_sections

    belongs_to :page

    scope :non_orderable, -> { where(position: 0).order(:name) }
    scope :orderable,     -> { where('position > 0').order(:position) }
    scope :published,     -> { where(published: true) }

    def activity_info
      "page #{page.title}, section #{name}"
    end

    def is_form?(prefix='')
      self.stype =~ /.*#{prefix}Form$/ ? true : false
    end

    def is_related_to_form?
      self.stype =~ /.*Form(Success|Error)$/ ? true : false
    end

    def is_a_group?
      self.stype =~ /.*GroupSection$/ ? true : false
    end

    def form_defaults
      {
        recipients: 'gaylec@cctsbaltimore.org',

        fields: [
          {
            input_label: "Search Term",
            input_name: "input_field",
            input_type: "text_field",
            input_validation: "presence"
          }
        ]
      }
    end

    def contact_form_defaults
      {
        recipients: 'gaylec@cctsbaltimore.org',

        fields: [
          {
            input_label: "Name",
            input_name: "name",
            input_type: "text_field",
            input_validation: "presence"
          },
          {
            input_label: "Email Address",
            input_name: "email",
            input_type: "text_field",
            input_validation: "presence"
          },
          {
            input_label: "Phone Number",
            input_name: "phone_number",
            input_type: "text_field",
            input_validation: ""
          },
          {
            input_label: "Message",
            input_name: "message",
            input_type: "text_area",
            input_validation: "presence"
          }
        ]
      }
    end

    def delete_form_field(deleted_field)
      form_fields = JSON.parse(content)
      form_fields["fields"].reject!{ |field| field['input_name'] == deleted_field }
      self.content = form_fields.to_json
    end

    def scrub(attr)
      send(attr).gsub(/ /, "_").gsub(/[^\d\w\-_]/, "")
    end

    def css_classify
      stype.underscore.gsub('_','-')
    end

    def css_idify
      scrub('name').underscore
    end

    private

    def set_defaults
      self.name = name || "Main"
      self.stype = stype || "Section"
      self.stype = scrub(:stype).classify

      self.content = content || contact_form_content || form_content || default_content

      self.position = position || 1
      self.published = published || true
      true
    end

    def default_content
      "<h1>#{name} Section Content</h1>"
    end

    def contact_form_content
      is_form?('Contact') ? contact_form_defaults.to_json : nil
    end

    def form_content
      is_form? ? form_defaults.to_json : nil
    end

    def add_related_form_sections
      return unless is_form?

      create_form_success_section
      create_form_error_section
    end

    def create_form_success_section
      section = Section.new(name: "#{name} Success",
                            stype: "#{stype}Success",
                            position: 0,
                            page_id: page_id)
      section.save!
    end

    def create_form_error_section
      section = Section.new(name: "#{name} Error",
                            stype: "#{stype}Error",
                            position: 0,
                            page_id: page_id)

      section.content = "<h1>We're Sorry!<h1>"
      section.content << "<p>Something went wrong, please try again.</p>"

      section.save!
    end
  end
end
