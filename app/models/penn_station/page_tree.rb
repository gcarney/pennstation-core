require 'rubytree'

module PennStation
  class PageTree < Tree::TreeNode
    def initialize(name = '/', page = Page.lookup('/'))
      super(name, page)
    end

    def build
      create_nodes
      sort_nodes
      self
    end

    private

    def create_nodes
      Page.where("url != '/'").order(:url).each do |page|
        add_page(page)
      end
    end

    def sort_nodes(node=self)
      child_list(node).sort!{|a,b| a.content.position <=> b.content.position }.each do |sorted_child|
        node << sorted_child
        sort_nodes(sorted_child)
      end
    end

    def child_list(node)
      node.children.map {|child| node.remove!(child) }
    end

    def add_page(page)
      url_components(page).inject(self) do |node, url_component|
        find_or_create(node, url_component, page)
      end
    end

    def url_components(page)
      page.url.split('/').reject{|e| e.empty? }
    end

    def find_or_create(node, name, page)
      begin
        node << PennStation::PageTree.new(name, page)
      rescue
        nil
      end

      node[name]
    end
  end
end
