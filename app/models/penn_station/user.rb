
# http://asciicasts.com/episodes/270-authentication-in-rails-3-1

module PennStation
  class User < Account
    has_many :roles, autosave: true

    accepts_nested_attributes_for :roles, allow_destroy: true

    def has_role?(*check_against)
      not (roles.map(&:name) & check_against).empty?
    end

    def activity_info
      "user #{name}"
    end
  end
end
