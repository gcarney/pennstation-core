module PennStation
  class Activity < ActiveRecord::Base
    validates :user, presence: true
    validates :info, presence: true
    validates :operation, presence: true

    def message
      "#{user} #{operation} #{info}"
    end
  end
end
