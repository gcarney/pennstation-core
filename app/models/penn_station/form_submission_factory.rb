
module PennStation
  class FormSubmissionFactory
    def initialize(page, values)
      @page, @values = page, values
    end

    def build
      FormSubmission.new(
        recipients: form_fields['recipients'],
        submitted_values: map_form_values.to_json,
        field_validations: form_validations
      )
    end

    private

    def map_form_values
      form_value_names.inject({}) do |obj, field|
        obj.merge({ field => @values['form_submission'].fetch(field, '') })
      end
    end

    def form_value_names
      form_fields['fields'].map{|h| h['input_name'] }
    end

    def form_validations
      form_fields['fields'].inject({}) do |obj, field|
        obj.merge({ field['input_name'] => field['input_validation'] })
      end
    end

    def form_fields
      form_section = @page.sections.find_by_stype(@values['form_submission']['stype'])
      form_section ? JSON.parse(form_section.content) : Hash.new( 'recipients' => 'NO.RECIPIENTS@test.com', 'fields' => [] )
    end
  end
end
