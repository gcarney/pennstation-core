module PennStation
  class Slideshow < ActiveRecord::Base
    validates :name, presence: true,
                     uniqueness: true

    has_many :slides, -> { order 'position' }

    def activity_info
      "slideshow #{name}"
    end
  end
end
