require_dependency "penn_station/application_controller"

module PennStation
  class ScheduledEventsController < ApplicationController
    before_action :set_scheduled_event, only: [:show, :edit, :update, :destroy]

#    track_user_activity

    def index
      @events = Event.order(:name)
    end

    def show
    end

    def new
      @scheduled_event = ScheduledEvent.new
    end

    def edit
    end

    def create
#{"utf8"=>"✓",
# "authenticity_token"=>"dpKzhpvDmhF4MxFvW1LdTWMDankJT+QuB8FPrFSIsVU=",
#
# "calendar"=>{"calendar_ids"=>["1"]},
#
# "scheduled_event"=>{"events"=>{"name"=>"ev1",
#                                "summary"=>"ev1 summ",
#                                "description"=>"<p>ev1 desc</p>"
#                    },
# "starts_at(2i)"=>"10",
# "starts_at(3i)"=>"19",
# "starts_at(1i)"=>"2013",
# "ends_at(2i)"=>"10",
# "ends_at(3i)"=>"20",
# "ends_at(1i)"=>"2013",
# "times"=>"1:00pm - 2:00pm",
#
# "locations"=>{"name"=>"ev1 loc",
#   "addresses"=>{"address1"=>"ev1 addr2",
#                 "city"=>"ev1 city",
#                 "state"=>"ev1 state",
#                 "zipcode"=>"ev1 zipcode",
#                 "directions"=>"ev1 directions"}
#                }
#   },
#
# "commit"=>"Save"}

      event = Event.new(event_params[:events])
      location = Location.new(location_params)
      location.address = Address.new(address_params)
      location.save!

      event.locations = [location]

      if event.save!
        Calendar.where(id: params['calendar']['calendar_ids']).each do |calendar|
          @scheduled_event = ScheduledEvent.new(scheduled_event_params)

          @scheduled_event.starts_at = Date.parse("#{params['scheduled_event']['starts_at(3i)']}-#{params['scheduled_event']['starts_at(2i)']}-#{params['scheduled_event']['starts_at(1i)']}") if params['scheduled_event']['starts_at(1i)']
          @scheduled_event.ends_at = Date.parse("#{params['scheduled_event']['ends_at(3i)']}-#{params['scheduled_event']['ends_at(2i)']}-#{params['scheduled_event']['ends_at(1i)']}") if params['scheduled_event']['ends_at(1i)']

          @scheduled_event.event = event

       #   @scheduled_event.calendar = calendar

       #   @scheduled_event.save
          calendar.scheduled_events += [@scheduled_event]
        end

        redirect_to scheduled_events_path, notice: "Event a_scheduled_event.event.name was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @scheduled_event.update(event_params)
        redirect_to edit_event_path(@scheduled_event), notice: "Successfully updated Event #{@scheduled_event.event.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @scheduled_event.destroy
      redirect_to events_url, notice: "Eventistrator #{@event.name} was successfully deleted."
    end

    private

    def set_scheduled_event
      @scheduled_event = ScheduledEvent.find(params[:id])
    end

    def event_params
      params.require(:scheduled_event).permit(events: [:name, :summary, :description])
    end

    def location_params
      params[:scheduled_event].require(:locations).permit(:name)
    end

    def address_params
      params[:scheduled_event][:locations].require(:addresses).permit(:address1, :address2, :city, :state, :zipcode, :directions)
    end

    def scheduled_event_params
      #params.require(:scheduled_event).permit(:starts_at, :ends_at, :times)
      params.require(:scheduled_event).permit(:times)
#      plist = params.require(:admin).permit(:name, :email, :password, :password_confirmation, roles_attributes: [:id,:name])
    end
  end
end
