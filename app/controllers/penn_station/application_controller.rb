module PennStation
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_filter :authenticate

    private

    def auth_token_key
      :auth_token_penn_station_user
    end

    def authenticate
      unless session[auth_token_key] and current_user
        redirect_to login_url + (params.has_key?(:reset) ? "?reset=#{params[:reset]}" : '')
      end
    end

    def current_user
      @current_user ||= User.find_by_auth_token(session[auth_token_key]) if session[auth_token_key]
    end
    helper_method :current_user

    def self.track_user_activity(model_instance_var = nil)
      after_filter(:only => [:create, :update, :destroy]) do |controller|
        model_instance_var ||= controller.controller_name.singularize
        Activity.create(
          :user      =>  @current_user.name,
          :operation => (controller.action_name == 'destroy' ? 'delete' : controller.action_name) + 'd',
          :info      => eval("@#{model_instance_var}").activity_info
        )
      end
    end
  end
end
