require_dependency "penn_station/application_controller"

module PennStation
  class FormFieldsController < ApplicationController
    before_action :set_page, only: [ :new, :show, :update ]

#    track_user_activity

    def new
#        @section = Section.new(page: @page)
    end

    def create
      # @section = Section.new(section_params)

      # if @section.save
      #   redirect_to edit_page_path(@section.page), notice: "Successfully created #{@section.name}."
      # else
      #   render action: 'new'
      # end
    end

    def edit
      # @section = Section.find(params[:id])
      # @page = @section.page
    end

    def update
      # @section = @page.sections.find params[:id]

      # if @section.update(section_params.merge(coerce_content(params)))

      #   # save or save and preview.

      #   if params[:save_op] == 'save'
      #     redirect_to edit_page_path(@page), notice: "Successfully updated section #{@section.name}."
      #   else
      #     redirect_to "#{@page.url}"
      #   end
      # else
      #   render action: 'edit'
      # end
    end

    def destroy
      @section = Section.find(params[:section_id])
      @page = @section.page

      @section.delete_form_field(params[:id])
      @section.save

      redirect_to edit_page_path(@page), notice: "Section #{@section.name} was successfully deleted."
    end

    private

    def set_page
      # @page = Page.find(params[:page_id] || section_params[:page_id])
    end

    def section_params
      # params.require(:section).permit(:name, :stype, :content, :page_id, :moreinfo_url)
    end

    def coerce_content(params)
      if params[:section][:content].kind_of?(Hash)
        # recipients =  params[:section][:content].delete('recipients')
        # { content: { "recipients" => recipients, "fields" => params[:section][:content].values }.to_json }
      # else
        # if params[:section].has_key?(:content)
        #   { content: params[:section][:content] }
        # else
        #   { content: @section.content }
        # end
      end
    end
  end
end
