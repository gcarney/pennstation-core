require_dependency "penn_station/application_controller"

module PennStation
  class EventsController < ApplicationController
    before_action :set_event, only: [:show, :edit, :update, :destroy]

    track_user_activity

    def index
      @events = Event.order(:name)
    end

    def show
    end

    def new
      @event = Event.new(
                 locations: [Location.new(address: Address.new)],
                 scheduled_events: [ScheduledEvent.new]
               )
    end

    def edit
    end

    def create
      @event = Event.new(event_params)
      assign_calendars

      if @event.save
        redirect_to event_path(@event), notice: "Event #{@event.name} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      assign_calendars

      if @event.update(event_params)
        redirect_to edit_event_path(@event), notice: "Successfully updated Event #{@event.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @event.destroy
      redirect_to events_url, notice: "Event #{@event.name} was successfully deleted."
    end

    private

    def set_event
      @event = Event.find(params[:id])
    end

    def scheduled_event_params
      params.require(:scheduled_events).permit(
                                          :id, :times,
                                          'starts_at(2i)', 'starts_at(3i)', 'starts_at(1i)',
                                          'ends_at(2i)', 'ends_at(3i)', 'ends_at(1i)'
                                        )
    end

    def event_params
      params.require(:event).permit(:id, :name, :summary, :description,
                                    scheduled_events_attributes: [
                                      :id, :times,
                                      'starts_at(2i)', 'starts_at(3i)', 'starts_at(1i)',
                                      'ends_at(2i)', 'ends_at(3i)', 'ends_at(1i)'
                                    ],
                                    locations_attributes: [
                                      :id, :name,
                                      address_attributes: [
                                        :id, :address1, :address2,
                                        :city, :state, :zipcode, :directions
                                      ]
                                    ]
                                   )
    end

    private

    def assign_calendars
      @event.scheduled_events.each do |se|
        if params['calendar'].present?
          se.calendars = Calendar.where(id: params['calendar']['calendar_ids']).to_a
        else
          se.calendars = []
        end
      end
    end
  end
end
