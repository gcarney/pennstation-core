require_dependency "penn_station/application_controller"

module PennStation
  class FormSubmissionsController < ApplicationController
    before_action :set_form_submission, only: [ :show, :destroy ]
    before_action :calc_form_count, only: [ :show, :index ]

    track_user_activity

    def index
      @form_submissions = FormSubmission.all.order('updated_at DESC')
    end

    def show
    end

    def destroy
      @form_submission.destroy
      redirect_to form_submissions_url, notice: "FormSubmissions #{@form_submission.id} was successfully deleted."
    end

    private

    def calc_form_count
      @form_count = PennStation::Section.where(stype: 'Form').count
    end

    def set_form_submission
      @form_submission = FormSubmission.find(params[:id])
    end

    def form_submission_params
      params.require(:form_submission).permit(:name)
    end
  end
end
