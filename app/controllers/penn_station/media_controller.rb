require_dependency "penn_station/application_controller"

module PennStation
  class MediaController < ApplicationController
    before_action :set_folder, only: [:show, :edit, :update, :destroy]

#    track_user_activity

    def index
      #@folders = Media.all.order(:name).includes(:uploaded_files)
      @media = Media.all
    end

    def show
    end

    def new
#      @folder = Media.new
    end

    def edit
    end

    def create
#      @folder = Media.new(folder_params)
#
#      if @folder.save
#        redirect_to @folder, notice: "Media #{@folder.name} was successfully created."
#      else
#        render action: 'new'
#      end
    end

    def update
#      if @folder.update(folder_params)
#        redirect_to @folder, notice: "Successfully updated #{@folder.name}."
#      else
#        render action: 'edit'
#      end
    end

    def destroy
#      @folder.destroy
#      redirect_to folders_url, notice: "Media #{@folder.name} was successfully deleted."
    end

    private

    def set_folder
#      @folder = Media.find(params[:id])
    end

    def folder_params
#      params.require(:folder).permit(:name)
    end
  end
end
