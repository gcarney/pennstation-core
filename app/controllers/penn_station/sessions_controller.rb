module PennStation
  class SessionsController < ActionController::Base
    skip_before_filter :authenticate, :only => [:new, :create]

    def new
      @title = title
      @sessions_path = set_sessions_path
      @password_resets_path = set_password_resets_path
    end

    def create
      user = user_model.find_by_email(params[:email])

      if user && user.authenticate(params[:password])
        session[auth_token_key] = user.auth_token

        respond_to do |format|
          #format.js { render :js => "window.location.href = '#{login_location}'" }

          format.html do
            redirect_to login_location, notice: "Email or password is incorrect"
          end
        end
      else
        #flash.now.alert = "Invalid email or password"
        #render "new"
        redirect_to login_location, alert: "Email or password is incorrect"
      end
    end

    def destroy
      session[auth_token_key] = nil
      redirect_to logout_location, notice: "Logged out!"
    end

    private

    def set_sessions_path
      sessions_path
    end

    def set_password_resets_path
      password_resets_path
    end

    def title
      'PennStation'
    end

    def user_model
      User
    end

    def auth_token_key
      :auth_token_penn_station_user
    end

    def login_location
      '/admin'
    end

    def logout_location
      '/admin'
    end
  end
end
