require_dependency "penn_station/application_controller"

module PennStation
  class UploadedFilesController < ApplicationController
    before_action :set_uploaded_file, only: [:show, :edit, :update, :destroy]

    track_user_activity

    def index
    end

    def create
      folder = Folder.where(name: params[:folder_id]).first
      folder = Folder.create(name: params[:folder_id]) if folder.nil?

      @uploaded_file = UploadedFile.new

      params[:file].original_filename = possible_alternate_filename

      @uploaded_file.file = params[:file]
      @uploaded_file.folder = folder

      if @uploaded_file.save
        redirect_to edit_folder_path(folder, format: 'html')
      else
        render "new"
      end
    end

    def edit
    end

    def update
      if @uploaded_file.update(uploaded_file_params)
        redirect_to edit_uploaded_file_path(@uploaded_file), notice: "Successfully updated File  #{@uploaded_file.label}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @uploaded_file.destroy
      redirect_to folder_path(@uploaded_file.folder), notice: "File #{@uploaded_file.label} was successfully deleted."
    end

    private

    def set_uploaded_file
      @uploaded_file = UploadedFile.find(params[:id])
    end

    def uploaded_file_params
      params.require(:uploaded_file).permit(:id, :label, :caption, :description, :folder_id,:file)
    end

    # A hook to override the original filename provided by the user.
    def possible_alternate_filename
      alt_filename = params.fetch(:alt_filename, params[:file].original_filename)
      unless alt_filename == params[:file].original_filename
        alt_filename += Pathname.new(params[:file].original_filename).extname
      end

      alt_filename
    end
  end
end
