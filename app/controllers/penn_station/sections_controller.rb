require_dependency "penn_station/application_controller"

module PennStation
  class SectionsController < ApplicationController
    before_action :set_page, only: [ :new, :create, :show, :update ]

    track_user_activity

    def new
      @section = Section.new(page: @page, stype: 'Section', published: true)
    end

    def create
      all_params = section_params.merge(position: num_sections(section_params[:page_id]))
      @section = Section.new(section_params.merge(all_params))

      if @section.save
        notice = "Successfully created #{@section.name}."
        update_page_visited_cookie 'section_properties'
        redirect_to edit_page_path(@section.page), notice: notice
      else
        render action: 'new'
      end
    end

    def edit
      @section = Section.find(params[:id])
      @page = @section.page
    end

    def update
      @section = @page.sections.find params[:id]

      if @section.update(section_params.merge(coerce_content(params)))

        update_page_visited_cookie section_params.has_key?(:content) ? 'section_content' : 'section_properties'

        # save or save and preview.

        if params[:save_op] == 'save'
          redirect_to edit_page_path(@page), notice: "Successfully updated section #{@section.name}."
        else
          redirect_to "#{@page.url}"
        end
      else
        render action: 'edit'
      end
    end

    def reorder
      #params => {"page"=>"2", "section"=>["43", "2", "29", "30", "34", "40", "44", "35"], "_"=>"1428601449436", "controller"=>"penn_station/sections", "action"=>"reorder"}

      p = PennStation::Page.find_by_id(params[:page])

      unless p.nil?
        p.sections.orderable.each do |section|
          new_position = section_index(params['section'], section)

          if new_position.present? and (section.position != new_position)
            section.position = new_position
            section.save
          end
        end
      end
    end

    def destroy
      @section = Section.find(params[:id])
      @page = @section.page
      @section.destroy
      redirect_to edit_page_path(@page), notice: "Section #{@section.name} was successfully deleted."
    end

    private

    def section_index(new_order, section)
      (index = new_order.index(section.id.to_s)).nil? ? nil : index+1
    end

    def set_page
      @page = Page.find(params[:page_id] || section_params[:page_id])
    end

    def section_params
      params.require(:section).permit(:name, :stype, :content, :page_id,
                                      :moreinfo_url, :published)
    end

    def num_sections(page_id)
      sections = Section.where(page_id: page_id)
      sections.present? ? sections.count : 0
    end

    def coerce_content(params)
      if params[:section][:content].kind_of?(Hash)
        recipients =  params[:section][:content].delete('recipients')
        fields = params[:section][:content]
        new_field = fields.delete('_new')

        if new_field['input_name'].present?
          fields.merge!({ new_field["input_name"] => new_field})
        end

        { content: { "recipients" => recipients, "fields" => fields.values }.to_json }
      else
        if params[:section].has_key?(:content)
          { content: params[:section][:content] }
        else
          { content: @section.content }
        end
      end
    end

    def update_page_visited_cookie(view)
      return unless @page.present?

      settings = JSON.parse(cookies.fetch(:page_edits, {}))
      settings[@page.url]['section'] = "section_#{@section.id}"
      settings[@page.url]['view'] = view
      cookies[:page_edits] = { value: settings.to_json, expires: 1.week.from_now, }

      @page_edit_cookies = JSON.parse(cookies[:page_edits])
    end

  end
end
