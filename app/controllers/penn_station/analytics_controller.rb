module PennStation
  class AnalyticsController < ApplicationController

    def index
      @analytics_setting = AnalyticsSetting.first
    end

    # only called via ajax
    def show
      @access_token = get_access_token
      render action: 'show', :layout  => false
      rescue => @ex
      render action: '_load_error', :layout  => false
    end

    def new
    end

    def edit
      @analytics_setting = AnalyticsSetting.find(params[:id])
    end

    def create
      oauth_authorization_code = analytic_params[:authorization_code]
      #logger.info oauth_authorization_code

      @access_token = oauth2_client.auth_code.get_token(oauth_authorization_code,
                                                        :redirect_uri => 'urn:ietf:wg:oauth:2.0:oob')
      #logger.info @access_token.inspect

      if @access_token.present?
        save_access_token(@access_token)
        redirect_to analytics_path, notice: "access token was successfully retrieved."
      else
        redirect_to analytics_path
      end
    end

    def update
      @analytics_setting = AnalyticsSetting.find(params[:id])

      if @analytics_setting.update(analytics_setting_params)
        redirect_to analytics_path, notice: "Successfully updated analytics settings."
      else
        render action: 'edit'
      end
    end

    def destroy
      # there should only be one analytics_setting record anyway, so ignore
      # the id parameter and clear the entire table
      AnalyticsSetting.delete_all
      redirect_to analytics_path, notice: "analytics settings have been reset."
    end

    private

    def get_access_token
      @analytics_setting = AnalyticsSetting.first

      if @analytics_setting
        access_token = OAuth2::AccessToken.from_hash oauth2_client, {:access_token => @analytics_setting.access_token,
                                                                     :refresh_token => @analytics_setting.refresh_token,
                                                                     :expires_at => @analytics_setting.expires_at}
        if access_token.expired?
          access_token = access_token.refresh!
          update_access_token(access_token)
        end

        access_token
      else
        nil
      end
    end

    def save_access_token(access_token)
      AnalyticsSetting.create(:access_token => access_token.token,
                              :refresh_token => access_token.refresh_token,
                              :expires_at => access_token.expires_at)
    end

    def update_access_token(access_token)
      analytics_setting = AnalyticsSetting.first
      analytics_setting.update(:access_token => access_token.token,
                               :refresh_token => access_token.refresh_token,
                               :expires_at => access_token.expires_at)
    end

    def oauth2_client
      client = OAuth2::Client.new(PennStation::AnalyticsSetting::OAUTH_CLIENT_ID,
                                  PennStation::AnalyticsSetting::OAUTH_CLIENT_SECRET, {
        :authorize_url => 'https://accounts.google.com/o/oauth2/auth',
        :token_url => 'https://accounts.google.com/o/oauth2/token'
      })
      return client
    end

    def analytic_params
      params.permit(:authorization_code)
    end

    def analytics_setting_params
      params.require(:analytics_setting).permit(:tracking_id, :tracking_code)
    end

  end
end

