module PennStation
  class PasswordResetsController < ApplicationController
    skip_before_filter :authenticate
    before_action :load_user, only: [ :edit, :update ]
    before_action :set_view_variables, only: [ :new, :edit ]

    def new
    end

    def create
      user = user_model.find_by_email(params[:email])

      message = if user
                  send_password_reset(user)
                  { notice: "An email was sent with password reset instructions. Check your spam folder if you don't see it soon." }
                else
                  { alert: "Email is incorrect" }
                end

      redirect_to login_location + '?reset=1', message
    end

    def edit
    end

    def update
      if @user.password_reset_sent_at < 2.hours.ago
        #flash.now.alert = "Password reset has expired."
        #render :edit
        redirect_to login_location + '?reset=1', alert: "Password reset has expired."
      else
        @user.password = user_params['password']
        @user.password_confirmation = user_params['password_confirmation']

        if @user.save
          respond_to do |format|
            #format.js { render :js => "window.location.href = '#{login_location}'" }

            format.html do
              redirect_to login_location
            end
          end
        else
          set_view_variables
          flash.now.alert = "Password reset failed."
          render :edit
        end
      end
    end

    private

    def set_view_variables
      @title = title
      @password_reset_path = set_password_reset_path
    end

    def load_user
      @user = user_model.find_by_password_reset_token!(params[:id])
    end

      # frontend user models must override these methods.

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    def user_model
      User
    end

    def login_location
      '/admin'
    end

    def send_password_reset(user)
      user.send_password_reset
    end

    def set_password_reset_path
      password_reset_path params[:id]
    end

    def title
      'PennStation'
    end
  end
end
