require_dependency "penn_station/application_controller"

module PennStation
  class CalendarsController < ApplicationController
    before_action :set_calendar, only: [:show, :edit, :update, :destroy]

    track_user_activity

    def index
      @calendars = Calendar.all.order(:name)
    end

    def show
    end

    def new
      @calendar = Calendar.new
    end

    def edit
    end

    def create
      @calendar = Calendar.new(calendar_params)

      if @calendar.save
        redirect_to @calendar, notice: "Calendar #{@calendar.name} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @calendar.update(calendar_params)
        redirect_to @calendar, notice: "Successfully updated #{@calendar.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @calendar.destroy
      redirect_to calendars_url, notice: "Calendar #{@calendar.name} was successfully deleted."
    end

    private

    def set_calendar
      @calendar = Calendar.find(params[:id])
    end

    def calendar_params
      params.require(:calendar).permit(:name)
    end
  end
end
