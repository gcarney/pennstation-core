require_dependency "penn_station/application_controller"

module PennStation
  class ActivitiesController < ApplicationController
    def index
      @activities = Activity.all.order("created_at desc")
    end
  end
end
