require_dependency "penn_station/application_controller"

module PennStation
  class PagesController < ApplicationController
    before_action :set_page, only: [ :show, :edit, :update, :destroy ]
    before_action :create_page_visited_cookie, only: [ :edit ]

    track_user_activity

    def index
      @workspace = "pages"
      @pages = ::PennStation::PageTree.new.build
    end

    def show
    end

    def new
      @page = Page.new(parent_url: page_params['parent_url'], ptype: 'Page')
    end

    def edit
    end

    def create
      @page = Page.new(page_params)

      if @page.save
        create_page_visited_cookie

        if params[:save_op] == 'save'
          redirect_to edit_page_path(@page), notice: "Successfully created #{@page.title}."
        else
          redirect_to "#{@page.url}"
        end
      else
        render action: 'new'
      end
    end

    def update
      update_page_visited_cookie 'page_properties'

      if @page.update(page_params)

          # save or save and preview.

        if params[:save_op] == 'save'
          redirect_to edit_page_path(@page), notice: "Successfully updated #{@page.title}."
        else
          redirect_to "#{@page.url}"
        end
      else
        render action: 'edit'
      end
    end

    def destroy
      @page.destroy
      redirect_to pages_url, notice: "Page #{@page.title} was successfully deleted."
    end

    def reorder

      # {"page"=>{"1"=>"root", "2"=>"1", "3"=>"2", "4"=>"2", "6"=>"2", "5"=>"2", "7"=>"2", "8"=>"1"}, "_"=>"1371679064349"}

      position_tracker = {}

      params['page'].each do |page_id,parent_id|
        next if parent_id == 'root'

        if position_tracker.has_key?(parent_id)
          position_tracker[parent_id] += 1
        else
          position_tracker[parent_id] = 1
        end

        page = PennStation::Page.find page_id.to_i
        parent = PennStation::Page.find parent_id.to_i
        page.parent_url = parent.url
        page.position = position_tracker[parent_id]
        page.save
      end
    end

    private

    def set_page
      @page = Page.includes(:sections).find(params[:id])
    end

    def create_page_visited_cookie(default_section_id=nil)
      return unless @page.present?

      section_id = default_section_id || @page.sections.first.id
      settings = begin
                   JSON.parse(cookies.fetch(:page_edits, {}))
                 rescue => e
                   {}
                 end

      unless settings.has_key? @page.url
        settings[@page.url] = {
                                section: "section_#{section_id}",
                                view: "page_properties"
                              }

        cookies[:page_edits] = { value: settings.to_json, expires: 1.week.from_now, }
      end

      @page_edit_cookies = JSON.parse(cookies[:page_edits])
    end

    def update_page_visited_cookie(view)
      return unless @page.present?

      settings = JSON.parse(cookies.fetch(:page_edits, {}))
      settings[@page.url] = {} unless settings.has_key? @page.url
      settings[@page.url]['view'] = view
      settings[@page.url]['section'] = "section_#{@page.sections.first.id}" unless settings[@page.url].has_key? 'section'
      cookies[:page_edits] = { value: settings.to_json, expires: 1.week.from_now, }

      @page_edit_cookies = JSON.parse(cookies[:page_edits])
    end

    def page_params
      params.require(:page).permit(:title, :ptype, :parent_url, :published,
                                   :show_in_nav, :seo_title, :seo_keywords,
                                   :seo_description, :show_share_widget,
                                   :password_protected)
    end
  end
end
