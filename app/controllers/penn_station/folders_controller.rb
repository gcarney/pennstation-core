require_dependency "penn_station/application_controller"

module PennStation
  class FoldersController < ApplicationController
    before_action :set_folder, only: [:show, :edit, :update, :destroy]

    track_user_activity

    def index
      @folders = Folder.all.order(:name).includes(:uploaded_files)
    end

    def show
    end

    def new
      @folder = Folder.new
    end

    def edit
    end

    def create
      @folder = Folder.new(folder_params)

      if @folder.save
        redirect_to @folder, notice: "Folder #{@folder.name} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @folder.update(folder_params)
        redirect_to @folder, notice: "Successfully updated #{@folder.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @folder.destroy
      redirect_to folders_url, notice: "Folder #{@folder.name} was successfully deleted."
    end

    private

    def set_folder
      @folder = Folder.find(params[:id])
    end

    def folder_params
      params.require(:folder).permit(:name)
    end
  end
end
