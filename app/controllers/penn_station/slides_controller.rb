require_dependency "penn_station/application_controller"

module PennStation
  class SlidesController < ApplicationController
    before_action :set_slide, only: [:show, :edit, :update, :destroy]

    track_user_activity

    def index
      @slides = Slide.all.order(:position)
    end

    def show
    end

    def new
      @slide = Slide.new
    end

    def edit
    end

    def create
      @slide = Slide.new#(slide_params)
      @slide.image = params["file"]
      @slide.title = params["file"].original_filename
      @slide.slideshow = Slideshow.where(name: params[:slideshow_id]).first

      if @slide.save
        redirect_to @slide, notice: "Slide #{@slide.title} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @slide.update(slide_params)
        redirect_to @slide, notice: "Successfully updated #{@slide.title}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @slide.destroy
      redirect_to edit_slideshow_path(@slide.slideshow), notice: "Slide #{@slide.title} was successfully deleted."
    end

    private

    def set_slide
      @slide = Slide.find(params[:id])
    end

    def slide_params
      params.require(:slide).permit(:title, :caption, :description, :url, :publish, :position)
    end
  end
end
