require_dependency "penn_station/application_controller"

module PennStation
  class UsersController < ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]

    track_user_activity

    def index
      @users = User.order(:name)
    end

    def show
    end

    def new
      @user = User.new
      generate_roles_list
    end

    def edit
      generate_roles_list
    end

    def create
      @user = User.new(user_params)

      if @user.save
        redirect_to @user, notice: "User #{@user.name} was successfully created."
      else
        generate_roles_list
        render action: 'new'
      end
    end

    def update
      if @user.update(user_params)
        redirect_to edit_user_path(@user), notice: "Successfully updated User #{@user.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @user.destroy
      redirect_to users_url, notice: "User #{@user.name} was successfully deleted."
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      plist = params.require(:user).permit(:name, :email, :password, :password_confirmation, roles_attributes: [:id,:name])

        # mark any associated roles that were unchecked for destruction.

      if plist.has_key?("roles_attributes")
        plist["roles_attributes"].each do |k,v|
          role = PennStation::Role.find_by_id(v['id'])
          next unless role
          if @current_user.has_role?(role.name)
            v["_destroy"] = true unless v.has_key?("name")
          end
        end
      end

      plist
    end

    def generate_roles_list
      PennStation::Role::LIST.each do |available_role|
        next if @user.has_role?(available_role)
        @user.roles.build(name: available_role)
      end
    end
  end
end
