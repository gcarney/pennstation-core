require_dependency "penn_station/application_controller"

module PennStation
  class SlideshowsController < ApplicationController
    before_action :set_slideshow, only: [:show, :edit, :update, :destroy]

    track_user_activity

    def index
      @slideshows = Slideshow.all.order(:name).includes(:slides)
    end

    def show
    end

    def new
      @slideshow = Slideshow.new
    end

    def edit
    end

    def create
      @slideshow = Slideshow.new(slideshow_params)

      if @slideshow.save
        redirect_to @slideshow, notice: "Slideshow #{@slideshow.name} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @slideshow.update(slideshow_params)
        redirect_to @slideshow, notice: "Successfully updated #{@slideshow.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @slideshow.destroy
      redirect_to slideshows_url, notice: "Slideshow #{@slideshow.name} was successfully deleted."
    end

    private

    def set_slideshow
      @slideshow = Slideshow.find(params[:id])
    end

    def slideshow_params
      params.require(:slideshow).permit(:name, :image_size_note)
    end
  end
end
