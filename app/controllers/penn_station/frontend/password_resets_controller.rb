module PennStation
  module Frontend
    class PasswordResetsController < PennStation::PasswordResetsController

      helper BreadcrumbHelper
      helper NavigationHelper
      helper PagesHelper
      helper HeaderHelper
      helper AnalyticsHelper
      helper RenderHelper
      helper SectionHelper

      def edit
        super
        @page = PennStation::Page.lookup(login_location[1..-1])
      end
    end
  end
end
