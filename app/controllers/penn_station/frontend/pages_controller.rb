module PennStation
  module Frontend
    class PagesController < PennStation::Frontend::ApplicationController
      protect_from_forgery with: :exception

      before_filter :load_page, only: [ :show ]
      before_filter :load_page_on_create, only: [ :create ]
      before_filter :authenticate

      def show
        @page = nil unless @page.present? && published_or_page_preview?
        render_missing_page unless @page.present?
      end

      # POSTing a Page means an incoming form submission.
      def create
        render_missing_page unless @page.present?

        @form_obj = FormSubmissionFactory.new(@page, params).build

        if security_test_passes && @form_obj.valid?
          @form_obj.save
          FormSubmissionMailer.notify_visitor(@form_obj).deliver_later
          FormSubmissionMailer.notify_administrators(@page.title, @form_obj).deliver_later
        else
          render action: 'show'
        end
      end

      private

      def auth_token_key
        portal_page = @page.portal
        "auth_token_#{portal_page.ptype.underscore.gsub('/','_')}".to_sym
      end

      def authenticate
        return unless @page.present?
        portal_page = @page.portal
        return unless portal_page.present?

        unless current_pennstation_user || (session[auth_token_key] && current_portal_user)
          redirect_to "#{portal_page.url}/login"
        end
      end

      def load_page
        @page = Page.lookup(params[:url])
      end

      def load_page_for_create
        @page = Page.find_by_url(params[:form_submission][:url])
      end

      def published_or_page_preview?
        @page.published || @current_pennstation_user
      end

      def security_test_passes
        @form_obj.fields_valid?(security_test_answer)
      end

      def security_test_answer
        session[:captcha_answer] == params[:form_submission][:captcha_answer].to_i
      end
    end
  end
end
