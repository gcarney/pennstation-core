module PennStation
  module Frontend
    class SearchController < ApplicationController
      def index
        search_term = params[:search_term]
        @page = Page.find_by_url('/search')

        if search_term.present?
          @results = Search.results(search_term)
          @num_results = @results.keys.inject(0) { |sum, key| sum + @results[key].size }
        else
          @search_error = "Please provide a search term."
        end
      end
    end
  end
end
