module PennStation
  module Frontend
    class SessionsController < PennStation::SessionsController

      helper BreadcrumbHelper
      helper NavigationHelper
      helper PagesHelper
      helper HeaderHelper
      helper AnalyticsHelper
      helper RenderHelper
      helper SectionHelper

      def new
        super
        @page = PennStation::Page.lookup(login_location[1..-1])
      end
    end
  end
end
