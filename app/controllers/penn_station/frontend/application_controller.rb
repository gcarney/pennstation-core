module PennStation
  module Frontend
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :exception
      before_filter :current_pennstation_user

        # NOTE: If these change, also update:
        #       app/controllers/penn_station/frontend/sessions_controller.rb
        #       app/controllers/penn_station/frontend/passsword_resets_controller.rb

      helper BreadcrumbHelper
      helper ::PennStation::NavigationHelper
      helper NavigationHelper
      helper ::PennStation::PagesHelper
      helper PagesHelper
      helper HeaderHelper
      helper AnalyticsHelper
      helper RenderHelper
      helper SectionHelper
      helper ::PennStationBlog::NavigationHelper
      helper ::PennStationBlog::PostsHelper

      private

      def current_pennstation_user
        @current_pennstation_user ||= User.find_by_auth_token(session[:auth_token_penn_station_user]) if session[:auth_token_penn_station_user]
      end
      helper_method :current_pennstation_user

      def current_portal_user
        keys = session.keys.select{|s| s.match /^auth_token_/}.reject{|r| r == "auth_token_penn_station_user"}
        return nil if keys.empty?

        key = keys.first
        user_klass = key.gsub('auth_token_','').classify
        user_klass.constantize.find_by_auth_token(session[key])
      end
      helper_method :current_portal_user

      def render_missing_page
        @page = PennStation::Page.lookup('/')
        render :layout => 'penn_station/frontend/missing_page', :template => 'penn_station/frontend/shared/missing_page'
      end
      helper_method :render_missing_page
    end
  end
end
