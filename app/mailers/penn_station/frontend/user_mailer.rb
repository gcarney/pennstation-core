module PennStation
  module Frontend
    class UserMailer < AccountMailer
      private

      def subject
        "#{organization} website password reset"
      end
    end
  end
end
