module PennStation
  class UserMailer < AccountMailer
    private

    def subject
      "#{defined?(::ORGANIZATION) ? ::ORGANIZATION : ''} PennStation password reset"
    end
  end
end
