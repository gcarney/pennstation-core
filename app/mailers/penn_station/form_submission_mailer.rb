module PennStation
  class FormSubmissionMailer < ActionMailer::Base
    def notify_administrators(title, info)
      @info = info
      mail(to: @info.recipients, from: @info.get_email, subject: "Website form received: #{title}")
    end

    def notify_visitor(info, subject = "Your message was received")
      @info = info
      mail(to: @info.get_email, from: @info.recipients, subject: subject)
    end
  end
end
