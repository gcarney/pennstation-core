module PennStation
  class AccountMailer < ActionMailer::Base
    default from: "noreply@cctsbaltimore.org"

    def password_reset(account)
      @account = account
      mail to: account.email, subject: subject
    end

    private

    def subject
      'AccountMailer subject'
    end
  end
end

