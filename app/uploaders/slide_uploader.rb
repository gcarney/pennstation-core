# encoding: utf-8

class SlideUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick

  storage :file

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "penn_station/folders/#{model.slideshow.name.gsub(' ','_')}"
  end

  version :thumb do
    process :resize_to_limit => [72, 38]
  end

  version :preview do
    process :resize_to_limit => [143, 143]
  end

  version :home do
    process :resize_to_limit => [581, 264]
  end

end
