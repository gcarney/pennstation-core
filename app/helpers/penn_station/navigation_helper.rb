
module PennStation
  module NavigationHelper
    EDIT_PAGE = "Edit this Page"
    BACKEND = "PennStation CMS v#{PennStation::VERSION}"
    SIGNOUT = 'Sign Out'

    def admin_toolbar(page)
      content_tag :div,
        (content_tag :nav,
           content_tag(:div, content_tag(:a, EDIT_PAGE, href: toolbar_page_edit(page))) +
           content_tag(:div, content_tag(:a, BACKEND, href: '/admin')) +
           content_tag(:div, content_tag(:a, SIGNOUT, href: "/admin/logout")) ,
         :id => 'pennstation_toolbar'),
        :class => 'full-width pennstation-toolbar'
    end

    def primary_nav(current_page, options={})
      return '' if current_page.nil?

      nav_options = { css_id: 'primary' }.merge(options)
      menu_items = build_menu(PennStation::NavTree.new.build, current_page)
      menu_ele = content_tag :ol, menu_items.join(" ").html_safe, class: 'menu'
      content_tag :nav, phone_menu_toggle + menu_ele, id: nav_options[:css_id]
    end

    def sitemap
      sitemap_items = build_sitemap(PennStation::NavTree.new.build)
      sitemap_ele = content_tag :ol, sitemap_items.join(" ").html_safe
      content_tag :nav, sitemap_ele, class: 'sitemap'
    end

    private

    def build_sitemap(nav_item)
      nav_item.children.map do |sub_sitemap|
        content_tag(:li, content_tag(:a, sub_sitemap.content.title, href: sub_sitemap.content.url) +
                         build_sub_sitemap(sub_sitemap))
      end
    end

    def build_sub_sitemap(sitemap_item)
      return "" unless sitemap_item.children.present?
      content_tag(:div, content_tag(:ol, build_sitemap(sitemap_item).join(" ").html_safe))
    end

    def toolbar_page_edit(page)
      if page.nil?
        edit_url='#'
      else
        edit_url = if ((page.ptype == 'blog') && defined?(PennStationBlog))
                     "/admin/blog"
                   else
                     "/admin/pages/#{page.id}/edit"
                   end
      end
    end

    def build_menu(nav_item, current_page)
      nav_item.children.map do |sub_menu|
        link_class = in_current_page_path?(sub_menu.content.url, current_page.url) ? 'current' : nil

        content_tag(:li, content_tag(:a, sub_menu.content.title, href: sub_menu.content.url) +
                         build_sub_menu(sub_menu, current_page),
                         class: link_class) +
        content_tag(:li, "", :class => 'separator')
      end
    end

    def build_sub_menu(menu_item, current_page)
      return "" unless menu_item.children.present?
      content_tag(:div, content_tag(:ol, build_menu(menu_item, current_page).join(" ").html_safe),
                   class: 'dropdown')
    end

    def in_current_page_path?(current_url, page_url)
      page_url.include?(current_url)
    end

    def phone_menu_toggle
      ("<input type='checkbox' id='menu_toggle'/>" +
       "<label for='menu_toggle' class='menu-toggle'>&#8801; Menu</label>").html_safe
    end
  end
end
