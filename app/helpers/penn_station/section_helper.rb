
module PennStation
  module SectionHelper
    def classes(section, seed = 'section')
      [ seed, section.css_classify ].uniq.join(' ')
    end
  end
end

