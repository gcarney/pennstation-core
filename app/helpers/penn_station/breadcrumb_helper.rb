
module PennStation
  module BreadcrumbHelper
    def breadcrumb_trail(page, html_options={}, separator=' > ', anchor=nil)
      content_tag(:nav, build(page, separator, anchor), html_options)
    end

    private

    def build(page, separator, anchor)
      [ ancestors(page), anchor(page, anchor) ].flatten.join(separator).html_safe
    end

    def ancestors(current_page)
      result = Array.new

      while current_page.has_parent? do
        current_page = PennStation::Page.find_by_url(current_page.parent_url)
        result.unshift(link_to(current_page.title, current_page.url))
      end

      result
    end

    def anchor(page, anchor)
      anchor.blank? ? [ page.title ] : [ link_to(page.title, page.url), anchor ]
    end
  end
end
