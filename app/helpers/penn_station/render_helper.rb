module PennStation
  module RenderHelper
    def find_page_partial(page)
      ptype = page.ptype.underscore

      [
        "layouts/pages/#{ptype}/page",
        "layouts/pages/page",
        "layouts/penn_station/frontend/pages/#{ptype}/page",
        "layouts/penn_station/frontend/pages/page"
      ].detect{ |path| lookup_context.exists?(path, [], true) }
    end

    def find_section_partial(section)
      ptype = section.page.ptype.underscore
      stype = section.stype.underscore
      partial_name = section.is_form? ? 'form' : 'section'

      [
        "layouts/pages/#{ptype}/#{stype}",
        "layouts/pages/#{ptype}/#{partial_name}",
        "layouts/pages/#{partial_name}",
        "layouts/penn_station/frontent/pages/#{ptype}/#{stype}/#{partial_name}",
        "layouts/penn_station/frontend/pages/#{ptype}/#{partial_name}",
        "layouts/penn_station/frontend/pages/#{partial_name}"
      ].detect{ |path| lookup_context.exists?(path, [], true) }
    end

    def find_shared_partial(partial)
      [
        "layouts/shared/#{partial}",
        "layouts/penn_station/frontend/shared/#{partial}"
      ].detect{ |path| lookup_context.exists?(path, [], true) }
    end

    def templater(content, template_data)
      return content if template_data.nil?

      template_data.keys.inject(content.clone) do |templated_content, key|
        templated_content.gsub!("{{#{key}}}","#{template_data[key]}")
        templated_content
      end
    end
  end
end

