module PennStation
  module HeaderHelper

    def seo_meta_tags
      meta_tags = []
      { 'keywords' => @page.seo_keywords,
        'description' => @page.seo_description }.each do |name, content|
        meta_tags << "<meta name=\"#{name}\" content=\"#{content}\">" unless content.blank?
      end
      meta_tags.join("\n").html_safe
    end

    # http://moz.com/learn/seo/title-tag
    def seo_title(page, org)
      "<title>#{org} | #{@page.seo_title || @page.title}</title>".html_safe
    end
  end
end
