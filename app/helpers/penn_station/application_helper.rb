module PennStation
  module ApplicationHelper
    def formatted_date(timestamp)
      timestamp.in_time_zone("EST").strftime("%Y-%b-%d %I:%M:%S%P %Z")
    end
  end
end
