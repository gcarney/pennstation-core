
module PennStation
  module CookiesHelper
    def cookie_section(page, value=nil)
      return '' if @page_edit_cookies.nil?

      if value.present?
        @page_edit_cookies[page.url]["section"] = value
      else
        @page_edit_cookies[page.url]["section"]
      end
    end

    def cookie_view_page_properties?(page)
      return true if @page_edit_cookies.nil?
      @page_edit_cookies[@page.url]["view"] == "page_properties"
    end

    def cookie_view_section_properties?(page)
      @page_edit_cookies[@page.url]["view"] == "section_properties"
    end

    def cookie_view_content?(page)
      @page_edit_cookies[@page.url]["view"] == "section_content"
    end

    def cookie_view_page_properties(page)
      @page_edit_cookies[@page.url]["view"] = "page_properties"
    end

    def cookie_view_section_properties(page)
      @page_edit_cookies[@page.url]["view"] = "section_properties"
    end

    def cookie_view_content(page)
      @page_edit_cookies[@page.url]["view"] = "section_content"
    end
  end
end
