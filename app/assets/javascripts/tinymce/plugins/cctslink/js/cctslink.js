/* Functions for the advlink plugin popup */

tinyMCEPopup.requireLangPack();

//Variables ------------------------------------------------
var templates    = { "window.open" : "window.open('${url}','${target}','${options}')" };
var link_types   = new Array('internal','external','uploaded_file','email');
var current_type = '';


//Functions ------------------------------------------------
function preinit() {
	var url;
	if (url = tinyMCEPopup.getParam("external_link_list_url")) document.write('<script language="javascript" type="text/javascript" src="' + tinyMCEPopup.editor.documentBaseURI.toAbsolute(url) + '"></script>');
}
function init() {
	tinyMCEPopup.resizeToInnerSize();

  //Declare/initialize variables -----------
  current_type = 'internal';
  var inst     = tinyMCEPopup.editor;
  var elm      = inst.selection.getNode();
  var href     = '';

  //Updating an existing link tag
	elm = inst.dom.getParent(elm, 'A');
	if (elm != null && elm.nodeName == 'A') {
    href = RemoveAbsoluteOrRelativePaths(elm.getAttribute('href'));

    //Get the type of the link
    if(elm.getAttribute('type')) {
      $(link_types).each(function(idx, type) {if(elm.getAttribute('type').indexOf(type)!=-1) current_type = type; });
    } else {
      //Legacy links - imply the type by the href attribute
      if(href.charAt(0) == '/') {
        current_type = href.include('uploaded_files') ? 'uploaded_file' : 'internal';
      } else if(href.include('mailto:')) {
        current_type = 'email';
      } else {
        current_type = 'external';
      }
    }
    href = PrepValuesForForm(href);
  }

  //Initialize form, tabs and panels -------
  InitializeForm(href);
  ToggleTypes();

  //Declare event observers ----------------
  $(link_types).each(function(idx, type) {
    $('#' + type + '_tab' + ' a').click(function(e) {
      //Use a regex (IE.href='complete url', FF.href='href')
      current_type = this.getAttribute('href').replace('#','');
      ToggleTypes();
    });
  });
  $('form').first().submit(function(e) {
    Insert();
  });
  $('#cancel').click(function(e) {
     tinyMCEPopup.close();
  });

  //Get pages and uploaded files -----------
  AjaxRequest('/admin/pages','internal_list');
  AjaxRequest('/admin/folders','uploaded_file_list');
}


function ToggleTypes() {
  //Toggle tabs
  $('.tabs li').removeClass('current');
  $('#' + current_type + '_tab').addClass('current');
  //Toggle panels
  $('.panel').removeClass('current');
  $('#' + current_type + '_panel').addClass('current');
}
function RemoveAbsoluteOrRelativePaths(href) {
  //IE:href = absolute_path
  href = href.replace(window.location.protocol+'//'+window.location.host,'');
  //FF:href = ../../../path
  if(href.charAt(0) == '.') href = '/' + href.gsub(/^\.\.\//,'');
  return href;
}
function InitializeForm(href) {
  $('#'+current_type+'_href').val(href);
  $('#href').val(href);

  if ((current_type == 'internal') && href.indexOf("#") != -1)
   {
     var href_arr = href.split("#");
     $('#' + current_type+'_href').val(href_arr[0]);
     $('#anchor').val(href_arr[1]);
   }
}
function PrepValuesForForm(href) {
//  if(href.charAt(0) == '.') href = '/' + href.gsub(/^(\.\.\/)/,''); //remove '../../../' but retain leading '/'
  if(href.charAt(0) == '.') href = '/' + href.replace(/^(\.\.\/)/,''); //remove '../../../' but retain leading '/'
  href = href.replace('mailto:','');   //Email begins with 'mailto:'
  return href;
}
function PrepValuesForInsertion() {
  $('#href').val($('#' + current_type + '_href').val());
  if(current_type == 'email') $('#href').val('mailto:' + $('#href').val());
  if(current_type == 'internal' && $('#anchor').val() != '') $('#href').val($('#href').val() + '#' + $('#anchor').val());
}

function AjaxRequest(path, container_id) {
//  var successCallback = function(xhr) {
//    $(container_id).update(xhr.responseText);
//  }
//  new Ajax.Request(path, {
//    method:     "get",
//    onSuccess:  successCallback.bind(this)
//  });

  $.ajax({beforeSend: function(xhr) { xhr.setRequestHeader("Accept", "text/javascript"); },
          type: "GET",
          url: path,
          dataType: 'text',
          success: function(data, status, xhr) {$('#'+container_id).empty().append(xhr.responseText)}
  })
}

function Insert() {
	var inst = tinyMCEPopup.editor;
	var elm, elementArray, i;

  PrepValuesForInsertion();

	elm = inst.selection.getNode();
	elm = inst.dom.getParent(elm, "A");

	// Remove element if there is no href
	if (!document.forms[0].href.value) {
		tinyMCEPopup.execCommand("mceBeginUndoLevel");
		i = inst.selection.getBookmark();
		inst.dom.remove(elm, 1);
		inst.selection.moveToBookmark(i);
		tinyMCEPopup.execCommand("mceEndUndoLevel");
		tinyMCEPopup.close();
		return;
	}

	tinyMCEPopup.execCommand("mceBeginUndoLevel");
	// Create new anchor elements
	if (elm == null) {
		tinyMCEPopup.execCommand("CreateLink", false, "#mce_temp_url#", {skip_undo : 1});

		elementArray = tinymce.grep(inst.dom.select("a"), function(n) {return inst.dom.getAttrib(n, 'href') == '#mce_temp_url#';});
		for (i=0; i<elementArray.length; i++) {
			elm = elementArray[i];
			// Move cursor to end
			try {
				tinyMCEPopup.editor.selection.collapse(false);
			} catch (ex) {
				// Ignore
			}
			setAllAttribs(elm);
		}
	} else {
		setAllAttribs(elm);
  }

	tinyMCEPopup.execCommand("mceEndUndoLevel");
	tinyMCEPopup.close();
}

function setAllAttribs(elm) {
  setAttrib(elm, 'href', $('href').value);
  setAttrib(elm, 'type', current_type);
}
function setAttrib(elm, attrib, value) {
	var formObj = document.forms[0];
	var valueElm = formObj.elements[attrib.toLowerCase()];
	var dom = tinyMCEPopup.editor.dom;

	if (typeof(value) == "undefined" || value == null) {
		value = "";
		if (valueElm) value = valueElm.value;
	}

	// Clean up the style
	if (attrib == 'style') value = dom.serializeStyle(dom.parseStyle(value));

	dom.setAttrib(elm, attrib, value);
}

// While loading
preinit();
tinyMCEPopup.onInit.add(init);
