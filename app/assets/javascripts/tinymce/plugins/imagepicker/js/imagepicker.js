
var UploadedImage = function(element){
  this.initialize(element);
}

UploadedImage.prototype = {
  initialize: function(element) {
    this.element = $(element);
    this.image   = this.element.find("img");
    this.name    = this.element.find(".name").text().replace(/^[\n,\s]*/, "").replace(/[\n,\s]*$/, "");
    this.size    = "medium";
    this.size_selector = this.element.find("ul");

    this.element.bind("mousedown", this.select.bind(this));

    var image = this;
    this.size_selector.find("li a").click(function(){
      image.size_selector.hide();
      image.submit(this);
      return false;
    });
    this.size_selector.hide();
  },
  url: function() {
    if (!this._url) {
      this._url = this.image.attr("src").replace(/\?\d+$/, "").split(/_tiny/);
    }
    return this._url[0] + "_" + this.size + this._url[1];
  },
  description: function() {
    if (!this._description) {
      this._description = this.image.attr("alt");
    }
    return (!this._description || this._description == "") ? this.name : this._description;
  },
  select: function(event) {
    this.markSelected();
    return false;
  },
  markSelected: function() {
    this.element.siblings().each(function(idx, ele) {
      $(ele).removeClass("selected");
      $(ele).find("ul").hide();
    });
    this.size_selector.show();
    this.element.addClass("selected");
  },
  submit: function(size_link) {
    this.size = size_link.getAttribute('href').replace(/.*#/, "");
    this.onSubmit();
  }
}


var ImageGalleriesPresenter = function(){
  this.initialize();
}

ImageGalleriesPresenter.prototype = {
  initialize: function(options) {
    this.buildBackground();
    this.root = this.buildRoot();
    $('body').append(this.root);
  },
  buildBackground: function() {
    this.background = jQuery("<div></div>").css({
      position: "absolute",
      width: "100%",
      height: "100%",
      top: "0",
      left: "0",
      opacity: 0.85,
      backgroundColor: "#fff"
    });
    this.background.bind('click', this.close.bind(this));
    $('body').append(this.background);
  },
  buildRoot: function() {
    return jQuery("<div></div>").attr('id', "image_picker_background").css({
      position:        "absolute",
      width:           "80%",
      height:          "80%",
      top:             "10%",
      left:            "10%",
      opacity:         1.0 
    });
  },
  open: function(event, bubble) {
    if (event && !bubble) { event.stop() }
    this.background.show();
    this.root.show();
  },
  close: function(event, bubble) {
    //if (event && !bubble) { event.stop() }
    this.root.hide();
    this.background.hide();
    return false;
  },
  show_window: function(element_or_html_text) {
    this.root.empty().append(element_or_html_text);
    $("#image_picker a.close").bind("click", this.reset_and_close.bind(this));

    var show_gallery = this.show_gallery.bind(this);
    $("#image_picker_gallery_list ul a").each(function(idx, link){
      $(link).click(function(){
        $.ajax({type: "GET",
                url: $(link).attr('href'),
                dataType: 'text',
                success: function(data, status, xhr) {
                           $("#image_picker_gallery_list ul a").removeClass('selected');
                           $(link).addClass('selected');
                           show_gallery(data);
                         }
        });
        return false;
      });
    });

    var reset_align_links = this.reset_align_links.bind(this);
    $("#image_picker_align_image a").each(function(idx, link){
      $(link).click(function(){
        var new_value = this.getAttribute('href').replace(/^(.)*#align_/, "");
        $("#align").get(0).value = new_value;
        reset_align_links(new_value);
        return false;
      });
    });
    this.reset_align_links(this.form("align"));

    this.form().bind("submit", this.capture_submit.bind(this));
    this.extendUploadedImages();
  },
  reset_and_close: function() {
    this.close();
    this.reset_align_links();
    this.form().find("#url").val(""); this.form().find("#alt").val("");
    return false;
  },
  reset_align_links: function(new_value) {
    new_value = new_value || "block";
    $("#image_picker_align_image a").each(function(idx, l) { $(l).removeClass("selected") });
    $("#image_picker_align_image a[href=#align_" + new_value + "]").first().addClass("selected");
  },
  form: function(element) {
    var f = $("#image_picker form");
    if (element) {
      return f.find("#"+element).val();
    } else {
      return f;
    }
  },
  capture_submit: function(event) {
    this.onSubmit( 
      this.form("url"),
      this.form("alt"),
      "image_" + this.form("align")
    );
    this.reset_and_close();
    return false;
  },
  show_gallery: function(element_or_html_text) {
    $("#image_picker_image_list").empty().append(element_or_html_text);
    this.extendUploadedImages();
  },
  extendUploadedImages: function() {
    $("#image_picker_image_list .uploaded_image").each(function(idx, div) {
      var i = new UploadedImage(div);
      i.onSubmit = function() {
        $("#url").get()[0].value = i.url();
        $("#alt").get()[0].value = i.description();
      }
    });
  },
}

var ImageGalleriesPlugin = function(){
  this.activate = function(editor){
    this.buildPresenter();
    this.presenter.open();
    this.editor = editor;
    $.ajax({type: "GET",
            url: '/admin/image_picker',
            data: this.buildRequestParams(),
            dataType: 'text',
            success: (function(data, status, xhr) {this.presenter.show_window(data);}).bind(this)
    })
  },

  this.buildPresenter = function(){
    if (this.presenter) { return }
    this.presenter = new ImageGalleriesPresenter();
    this.presenter.onSubmit = this.insertImage.bind(this);
  },

  this.buildRequestParams = function() {
    var url = "", alt = "", align = "", element = this.editor.selection.getNode();
    if (this.editor.selection.getNode().nodeName == "IMG") {
      url   = this.editor.dom.getAttrib(element, "src");
      alt   = this.editor.dom.getAttrib(element, "alt");
      align = this.editor.dom.getAttrib(element, "class").replace(/image_/, "");
    }
    return {
      image_url: url,
      image_alt: alt,
      image_align: $(["right", "left"]).has(align) ? align : "block"
    }
  },

  this.insertImage = function(url, description, class_names) {
    this.editor.execCommand('mceInsertContent', false, this.editor.dom.createHTML("img", {
      src: url,
      alt: description,
      "class": class_names
    }));
  }
}

