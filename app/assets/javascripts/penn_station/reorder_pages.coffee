jQuery ->
  jQuery("#pages_sortable.sortable").nestedSortable
    handle: "div"
    isTree: true
    items: "li"
    toleranceElement: "> div"
    update: (event, ui) ->
      jQuery.ajax
        evalScripts: true
        url: "/admin/pages/reorder"
        type: "GET"
        data: jQuery("#pages_sortable.sortable").nestedSortable("serialize")
        dataType: "script"

        # beforeSend: function(ev,xhr) {
        #   alert("das update" + jQuery('.sortable').nestedSortable('serialize'));
        # },
        # error: function(jqXHR, textStatus, errorThrown) {
        #   alert(textStatus);
        #   alert("There was a problem reordering the list.  Please refresh the page and try again.");
        # }
        complete: ->
      return

  #jQuery("#panel_tabsx .sortable").nestedSortable
  jQuery("#panel_tabsx .sortable").sortable
    update: (event, ui) ->
      jQuery.ajax
        evalScripts: true
        url: "/admin/sections/reorder" + "?page=" + jQuery('#pages').data('page-id')
        type: "GET"
        data: jQuery("#panel_tabsx .sortable").sortable("serialize")
        dataType: "script"

        #beforeSend: (ev,xhr) ->
        #  alert "das section update" + jQuery('#panel_tabsx .sortable').sortable("serialize")

        # error: function(jqXHR, textStatus, errorThrown) {
        #   alert(textStatus);
        #   alert("There was a problem reordering the list.  Please refresh the page and try again.");
        # }
        complete: ->

