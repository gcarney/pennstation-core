jQuery ->
  analytics_setting_id = jQuery("#analytics_id").attr("data-id")

  unless analytics_setting_id is `undefined`
    jQuery.ajax
      type: "GET"
      url: "/admin/analytics/" + analytics_setting_id
      dataType: "html"
      success: (data, status, xhr) ->
        $("#analytics_data").empty().append data

