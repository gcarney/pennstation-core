(($) ->
  $ ->

    portalSelector = ".js-portal"
    panelSelector  = '.js-flip-panels'

    doEffect = (transform) ->
      $(panelSelector).css "transform", transform

    oldsetFocus = (panel) ->
      $("#{panelSelector} #{panel} input#email").focus()

    setFocus = ->
      $("#{panelSelector} input#email")

    $("#{panelSelector} div:first-of-type a").click (event) ->
      event.stopPropagation()
      doEffect "rotateY(180deg)"
      setFocus().last().focus()

    $("#{panelSelector} div:last-of-type a").click (event) ->
      event.stopPropagation()
      doEffect ""
      setFocus().first().focus()

) jQuery
