(($) ->
  $ ->
    $("#pennstation #panel_chooser li").click ->
      $('#pennstation #panel_chooser li').removeClass 'current'
      $(this).addClass 'current'

      $('#pennstation .panels .panel').removeClass 'current'
      new_panel_id = "##{$(this).data('panel-id')}"

      if new_panel_id == '#page_properties'
        $('#sections_list').hide()
      else
        new_panel_id = "##{$('#panel_tabsx li.current').data('panel-id')} .#{$(this).data('panel-id')}"

        $(new_panel_id).siblings().removeClass 'current'
        $(new_panel_id).parent().addClass 'current'

        $('#sections_list').show()

      $(new_panel_id).addClass 'current'

      cookieValue = $.parseJSON($.cookie('page_edits'))
      pageUrl = $('#pages').data('page-url')
      cookieValue[pageUrl]['view'] = $(this).data('panel-id')
      $.cookie('page_edits', JSON.stringify(cookieValue))

      event.stopPropagation()

    $("#pennstation #panel_actions li").click ->
      panel = $("#pennstation #panel_tabsx li.current").data("panel-id")
      target = $('#panel_chooser li.current').data('panel-id')
      formSelector = if target == 'page_properties'
                       "#pennstation .panels ##{target}.current form"
                     else
                       "#pennstation .panels ##{panel}.current .#{target}.current form"

      form = $(formSelector).first()
      input = $("<input>").attr("type", "hidden").attr("name", "save_op").val($(this).attr('id'))

      form.append $(input)
      form.submit()
      event.stopPropagation()

    $("#pennstation #panel_tabsx li").click ->
      panel = $(this).data("panel-id")
      target = $('#panel_chooser li.current').data('panel-id')

      $("#pennstation #panel_tabsx li.current").removeClass "current"
      $("#pennstation .panels .current").removeClass("current")

      $("#pennstation .panels ##{panel}").addClass("current")
      $("#pennstation .panels ##{panel} .#{target}").addClass("current")

      cookieValue = $.parseJSON($.cookie('page_edits'))
      pageUrl = $('#pages').data('page-url')
      cookieValue[pageUrl]['section'] = $(this).attr('id')
      $.cookie('page_edits', JSON.stringify(cookieValue))

      $(this).toggleClass "current"

) jQuery
