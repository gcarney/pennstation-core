PennStation::Engine.routes.draw do

  root :to => 'pages#index'

  get "logout" => "sessions#destroy", :as => "logout"
  get "login"  => "sessions#new",     :as => "login"

  resources :activities
  resources :users
  resources :analytics
  resources :calendars
  resources :calendar_subscriptions
  resources :events
  resources :form_submissions

  resources :folders do
    collection do
      get :images_for_imagepicker
    end
  end

  resources :image_picker, :only => [:index, :show]

  resources :media

  resources :pages do
    collection do
      get :reorder
    end
  end

  resources :password_resets
  resources :scheduled_events

  resources :sections do
    collection do
      get :reorder
    end

    resources :form_fields
  end

  resources :sessions

  resources :slides do
    collection do
      get :images_for_imagepicker
    end
  end

  resources :slideshows
  resources :uploaded_files

end
