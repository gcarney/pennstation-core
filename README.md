PennStation 4
=============

The PennStation CMS Rails engine.

Pre-requisites
--------------

  - OS packages
    - g++
    - mysql-server mysql-client libmysqlclient-dev
    - libxslt-dev libxml2-dev # for nokogiri
    - libsqlite3-dev
    - imagemagick libmagickcore-dev libmagickwand-dev
    - zlib1g-dev
    - libreadline-dev
    - libssl-dev
  - ruby 2.0.0 - see .ruby-version for the specific version.
  - The bundler gem

Preparing for development work
------------------------------

    $ bundle install --path vendor --binstubs .bundle/bin
    $ cd test/dummy
    $ bundle exec rake penn_station:install:migrations
    $ bundle exec rails g pennstation:bootstrap
    $ bundle exec rake db:migrate RAILS_ENV=test
    $ cp ../../config/tinymce.yml config
    $ cd ../..

    # Run the tests:
    $ bundle exec rake

Exploring PennStation
---------------------

  Run these commands to launch the dummy app:

    $ cd test/dummy
    $ bundle exec rake db:seed
    $ bundle exec rails s -p 3009

  Visit http://localhost:3009/admin in your browser.

