namespace :penn_station do
  desc "Copy tinymce non-asset files"
  task :copy_tinymce_files do
    puts "Running: cp -r #{PennStation::Engine.root}/lib/tinymce/* $TINYMCE_DEST"
    puts `cp -r #{PennStation::Engine.root}/lib/tinymce/* $TINYMCE_DEST`

    puts "Running: cp $TINYMCE_DEST/jquery.js $TINYMCE_DEST/.."
    puts `cp $TINYMCE_DEST/jquery.js.raw $TINYMCE_DEST/../jquery.js`
  end
end
