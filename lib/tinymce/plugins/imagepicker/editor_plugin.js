(function() {

  tinymce.create("tinymce.plugins.ImagePickerPlugin", {
    init : function(ed, url) {
      var plugin = new ImageGalleriesPlugin();
      ed.addCommand("openImagePicker", function() {
        plugin.activate(ed)
      });
      // default tinymce image insert button is named 'image'
      // override it with our own imagepicker plugin
      ed.addButton("image", {
        title: "Insert an image",
        cmd:   "openImagePicker" 
      });
      ed.onNodeChange.add(function(ed, cm, n) {
        cm.setActive("imagepicker", n.nodeName == "IMG")
      })
    },

    getInfo : function() {
      return {
        longname:  "Image picker plugin",
        author:    "CCTS",
        authorurl: "http://ccts.ubalt.edu",
        infourl:   "none",
        version:   "0.1"
      }
    }
  });

  tinymce.PluginManager.add("imagepicker", tinymce.plugins.ImagePickerPlugin);

})();
