PennStation.configure do |config|
end

module PennStation

    # tabs in the backend will appear in the order given here.

  CLIENT_MODELS = %w()

    # emails for password reset will be sent with this sender address.

  PASSWORD_RESET_FROM_ADDRESS = "Change This <test@test.com>"

    # max sizes for uploaded files.

  UPLOADED_FILE_MAX_SIZE = 45.megabytes.to_i
  SLIDE_MAX_SIZE = 5.megabytes.to_i

    # Set a regular expression to keep search from hitting protected pages
    # unless told to.  For example, "/trustees%"

  PROTECTED_PAGES = ""

  CLIENT_ACCOUNTS = %w()
end

