class PagesController < ApplicationController

  def show
    @page = PennStation::Page.lookup(params[:url])
    @page = nil unless @page.present? && published_or_page_preview?

    render_missing_page unless @page.present?
  end

  # POSTing a Page means an incoming form submission
  def create
    @page = PennStation::Page.find_by_url(params[:form_submission][:url])
    render_missing_page unless @page.present?

    @form_obj = PennStation::FormSubmissionFactory.new(@page, params).build

    if security_test_passes && @form_obj.valid?
      @form_obj.save
      PennStation::FormSubmissionMailer.notify_visitor(@form_obj).deliver
      PennStation::FormSubmissionMailer.notify_administrators(@page.title, @form_obj).deliver
    else
      render action: 'show'
    end
  end

  private

  def published_or_page_preview?
    @page.published || @current_user
  end

  def security_test_passes
    @form_obj.fields_valid?(security_test_answer)
  end

  def security_test_answer
    session[:captcha_answer] == params[:form_submission][:captcha_answer].to_i
  end
end
