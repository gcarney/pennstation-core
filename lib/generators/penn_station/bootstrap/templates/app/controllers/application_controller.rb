class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :current_user

  helper ::PennStation::BreadcrumbHelper
  helper ::PennStation::NavigationHelper
  helper ::PennStation::HeaderHelper
  helper ::PennStation::AnalyticsHelper
  helper ::PennStation::RenderHelper
  helper ::PennStation::SectionHelper

  private

  def current_user
    @current_user ||= PennStation::User.find_by_auth_token(session[:auth_token]) if session[:auth_token]
  end
  helper_method :current_user

  def render_missing_page
    render :layout => 'missing_page', :template => 'shared/missing_page'
  end
  helper_method :render_missing_page
end
