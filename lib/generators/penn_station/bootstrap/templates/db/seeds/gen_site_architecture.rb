
require 'yaml'

# example of site architecture yaml file:
#

page_list = 'db/seeds/site_architecture.yml'

puts "\nAdding Pages"

def urlized_title(page)
  page['title'].gsub(/ /, "_").gsub(/[^\d\w\-_]/, "").downcase
end

def calc_url(page, parent)
  if page.fetch('ptype','') == 'HomePage'
    '/'
  elsif parent.fetch('ptype','') == 'HomePage'
    "/#{urlized_title(page)}"
  else
    "#{parent['url']}/#{urlized_title(page)}"
  end
end

def create_page(page, parent)
  puts "\nCreating page:"
  page['url'] = calc_url(page, parent)

  new_page = PennStation::Page.where(url: page['url']).first_or_create do |page_rec|
    page_rec.parent_url = page['url'].split('/')[0..-2].join('/')
    page_rec.parent_url = "/" if page_rec.parent_url.empty?

    page_rec.title = page['title']
    page_rec.ptype = page['ptype']
    page_rec.position = page['position']
    page_rec.show_in_nav = page['show_in_nav']
    page_rec.published = page['published']
  end

  new_page.save

  puts "  id: #{new_page.id}"
  puts "  title: #{new_page.title}"
  puts "  ptype: #{new_page.ptype}"
  puts "  url: #{new_page.url}"
  puts "  position: #{new_page.position}"
  puts "  published: #{new_page.published}"
  puts "  show_in_nav: #{new_page.show_in_nav}"

  unless page.fetch('sections', []).empty?
    section_position = 1
    puts "  sections:"

    page['sections'].each do |section|
      new_section = PennStation::Section.where(page_id: new_page.id)
                                        .where(name: section['name'])
                                        .where(stype: section['stype'])
                                        .first_or_create do |section_rec|

        section_rec.published = section['published']
      end

      unless new_section.is_related_to_form?
        new_section.position = ++section_position
        section_position += 1
      end

      new_section.save

      puts "    name: #{new_section.name}"
      puts "    stype: #{new_section.stype}"
      puts "    position: #{new_section.position}"
      puts "    page_id: #{new_section.page_id}"
      puts "    published: #{new_section.published}"
      puts ""
    end
  end

  return if page.fetch('children', []).empty?

  page['children'].each_with_index do |child, index|
    child['position'] = index+1
    create_page(child, page)
  end
end

pages = YAML.load(File.open(page_list))

pages.each_with_index do |page, index|
  page['position'] = index+1
  create_page(page, Hash.new)
end
