
puts "\nSeeding PennStation Core"

user_accounts = [
  {
    name: 'Gayle Carney',
    email: 'gaylec@cctsbaltimore.org',
  },
  {
    name: 'Charles Melhorn',
    email: 'charlesm@cctsbaltimore.org',
  },
  {
    name: 'Jack Waugh',
    email: 'jackw@cctsbaltimore.org',
  },
  {
    name: 'Warren Vosper',
    email: 'straydogsw@gmail.com',
  },
  {
    name: 'Melina McLean',
    email: 'melinakorine@gmail.com',
  },
  {
    name: 'Connor McCrone',
    email: 'conman246@gmail.com',
  },
  {
    name: 'David Grimm',
    email: 'grimmoutlook87@gmail.com',
  },
  {
    name: 'Chris Guzman',
    email: 'guzmantchris@gmail.com',
  },
  {
    name: 'Seth Swenson',
    email: 'swensonsd@gmail.com',
  },
]

puts "\nAdding Users"
user_accounts.each do |info|
  an_admin = PennStation::User.where(:email => info[:email]).first_or_create do |admin|
    puts "\n  Adding PennStation user #{info[:name]}"
    admin.email = info[:email]
    admin.name = info[:name]
  end

  PennStation::Role::LIST.each do |role|
    PennStation::Role.where(:name => role).where(:user_id => an_admin.id).first_or_create do |a_role|
      puts "    Adding PennStation role: #{role} to #{an_admin.name}"
      an_admin.roles << a_role
    end
  end
end
