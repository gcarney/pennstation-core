
require 'yaml'

def dump_pages(node, indent='')
  page = node.content

  puts indent+"-"
  puts indent+"  title: #{page.title}"
  puts indent+"  ptype: #{page.ptype}"
  puts indent+"  published: #{page.published}"
  puts indent+"  show_in_nav: #{page.show_in_nav}"

  puts indent+"  sections:"
  page.sections.each do |section|
    puts indent+"    -"
    puts indent+"      name: #{section.name}"
    puts indent+"      stype: #{section.stype}"
    puts indent+"      published: #{section.published}"
    puts indent+"      content: #{section.content}" if section.stype =~ /Form$/
  end

  return if node.children.empty?

  puts indent+"  children:"
  node.children.sort_by{|s| s.content.position}.each do |child|
    dump_pages(child, indent+'    ')
  end
end

t = PennStation::PageTree.new.build
dump_pages(t.root)

