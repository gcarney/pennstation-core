ENV["RAILS_ENV"] = "test"

require 'test_helper'
require 'database_cleaner'
require File.expand_path("../config/environment.rb",  __FILE__)

DatabaseCleaner.strategy = :transaction

class MiniTest::Spec
  before :each do
    DatabaseCleaner.start
  end

  after :each do
    DatabaseCleaner.clean
  end
end

