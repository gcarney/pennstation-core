require 'simplecov'
SimpleCov.start 'rails'  do
  add_filter "/vendor/ruby/"
end

require "minitest/spec"
require "minitest/autorun"

