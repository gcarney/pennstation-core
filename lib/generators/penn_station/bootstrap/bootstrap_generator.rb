module PennStation
  module Generators
    class BootstrapGenerator < Rails::Generators::Base
      source_root File.expand_path("../templates", __FILE__)
      desc "Bootstrap PennStation in a new client project"

      def copy_skeleton
        directory './', './'
      end

      def run_all
        puts "\nInstalling migrations"
        puts `rake penn_station:install:migrations`

        puts "\nMigrating database"
        puts `rake db:migrate`

        puts "\nSeeding database"
        puts `rake db:seed`
      end

      def complete
        puts "\nPennStation install complete!"
      end
    end
  end
end
