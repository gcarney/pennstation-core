
# inspired by:
# https://github.com/ryanb/nifty-generators/blob/master/lib/generators/nifty/scaffold/scaffold_generator.rb

require 'rails/generators/migration'

class PennStation::Scaffold < Rails::Generators::Base
  include Rails::Generators::Migration

  no_tasks { attr_accessor :model_name, :model_attributes }

  source_root File.expand_path('../templates', __FILE__)
  desc "Generate PennStation backend scaffolding for an existing client-side model"
  argument :model_name, :type => :string, :required => true, :banner => 'ModelName'
  argument :args_for_m, :type => :array, :default => [], :banner => 'model:attributes'

  def initialize(*args, &block)
    super

    @model_attributes = []
    args_for_m.each do |arg|
      @model_attributes << Rails::Generators::GeneratedAttribute.new(*arg.split(':'))
    end
    @model_attributes.uniq!
  end

  def generate_migration
    migration_template 'migration.rb', "db/migrate/create_#{model_name.pluralize.gsub('/', '_')}.rb"
  end

  def generate_model
    template 'model.rb', "app/models/#{model_name}.rb"
    template "test/model.rb", "test/models/#{model_name}_test.rb"
  end

  def generate_controller
    template 'controller.rb', "app/controllers/penn_station/#{plural_name}_controller.rb"
    template "test/controller.rb", "test/functional/penn_station/#{plural_name}_controller_test.rb"

    namespaces = plural_name.split('/')
    resource = namespaces.pop
    route namespaces.reverse.inject("resources :#{resource}") { |acc, namespace|
      "  namespace(:#{namespace}){ #{acc} }"
    }
  end

  def generate_views

      # the key is the model's database type and the value is the html input type.
      # if the db type is not present in the hash, the default input type is 'text'.

    @db2view_type_mapper = {
      "text": "text_area"
    }
    @db2view_type_mapper.default = "text"

    %w(
      edit.html.erb
      _form.html.erb
      index.html.erb
      new.html.erb
      show.html.erb
    ).each do |view_file|
      template "views/#{view_file}", "app/views/penn_station/#{plural_name}/#{view_file}"
    end
  end

  def update_ps_backend_menu

      # add to the end of the CLIENT_MODELS array.

    gsub_file 'config/initializers/penn_station.rb', /(CLIENT_MODELS[^\)]+)/, "\\1 #{model_name} "
  end

  def complete
    puts ""
    puts "PennStation backend code generated for model '#{model_name}'."
    puts "Be sure to stop the rails server and run the following commands:"
    puts "  $ bundle exec rake db:migrate"
    puts "  $ bundle exec rails server"
    puts ""
  end

  private

  def table_name
    plural_name.gsub('/', '_')
  end

  def plural_name
    model_name.underscore.pluralize
  end

  def singular_name
    model_name.underscore
  end

  def class_name
    model_name.camelize
  end

  def plural_class_name
    plural_name.camelize
  end

  def self.next_migration_number(path)
    unless @prev_migration_nr
      @prev_migration_nr = Time.now.utc.strftime("%Y%m%d%H%M%S").to_i
    else
      @prev_migration_nr += 1
    end
    @prev_migration_nr.to_s
  end
end
