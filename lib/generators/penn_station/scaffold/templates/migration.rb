class Create<%= class_name.pluralize.delete('::') %> < ActiveRecord::Migration
  def self.up
    create_table :<%= table_name || plural_name.split('/').last %> do |t|
    <%- model_attributes.each do |attribute| -%>
      t.<%= attribute.type %> :<%= attribute.name %>
    <%- end -%>

      t.timestamps
    end
  end

  def self.down
    drop_table :<%= table_name || plural_name.split('/').last %>
  end
end
