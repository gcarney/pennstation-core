module PennStation
  class <%= plural_class_name %>Controller < ApplicationController
    before_action :set_<%= singular_name %>, only: [:show, :edit, :update, :destroy]

    track_admin_activity

    def index
      @<%= plural_name %> = <%= class_name %>.all.order(:name)
    end

    def show
    end

    def new
      @<%= singular_name %> = <%= class_name %>.new
    end

    def edit
    end

    def create
      @<%= singular_name %> = <%= class_name %>.new(<%= singular_name %>_params)

      if @<%= singular_name %>.save
        redirect_to @<%= singular_name %>, notice: "<%= singular_name %> #{@<%= singular_name %>.name} was successfully created."
      else
        render action: 'new'
      end
    end

    def update
      if @<%= singular_name %>.update(<%= singular_name %>_params)
        redirect_to @<%= singular_name %>, notice: "Successfully updated #{@<%= singular_name %>.name}."
      else
        render action: 'edit'
      end
    end

    def destroy
      @<%= singular_name %>.destroy
      redirect_to <%= plural_name %>_url, notice: "<%= singular_name %> #{@<%= singular_name %>.name} was successfully deleted."
    end

    private

    def set_<%= singular_name %>
      @<%= singular_name %> = <%= class_name %>.find(params[:id])
    end

    def <%= singular_name %>_params
      params.require(:<%= singular_name %>).permit(:name)
    end
  end
end

