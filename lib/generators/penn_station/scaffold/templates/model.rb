
class <%= class_name %> < ActiveRecord::Base

  def activity_info
    "<%= singular_name %> #{id}"
  end
end
