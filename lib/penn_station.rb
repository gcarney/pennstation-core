require "penn_station/engine"
require "penn_station/version"
require 'tinymce-rails'
require 'dropzonejs-rails'
require 'sass/rails'
require 'carrierwave'
require 'carrierwave_patch'
require 'oauth2'
require 'legato'

module PennStation
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  class Configuration
    attr_accessor :option

    def initialize
      @option = 'default_option'
    end
  end
end
