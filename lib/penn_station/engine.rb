module PennStation
  class Engine < ::Rails::Engine
    isolate_namespace PennStation

    config.generators do |g|
      g.test_framework :minitest, :fixture => false
      g.assets false
      g.helper false
    end

  end
end
